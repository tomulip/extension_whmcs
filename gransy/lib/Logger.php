<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * Class for logging to WHMCS module log
 */
class Logger
{
    /**
     * Registrar module string
     */
    const GRANSY_MODULE = 'gransy_registrar';

    /**
     * Logs message to WHMCS module log
     *
     * @see https://developers.whmcs.com/provisioning-modules/module-logging/
     *
     * @param string $action
     * @param array|string $data
     * @param array|string $response
     * @param string $module
     */
    public static function log($action, $data, $response, $module = self::GRANSY_MODULE)
    {
        logModuleCall(
            $module,
            $action,
            $data,
            null,
            $response,
            [
                $data['data']['login'],
                $data['data']['password'],
                $data['data']['authid'],
                $data['data']['ssid'],
                $data['data']['whmcs_params']['Username'],
                $data['data']['whmcs_params']['Password']
            ]
        );
    }
}