<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * General SOAP request class
 */
class SoapRequest
{

    /**
     * Available functions of the SOAP server
     */
    const LOGIN_FUNCTION = 'Login';
    const PRICES_FUNCTION = 'Prices';
    const CHECK_DOMAIN_FUNCTION = 'Check_Domain';
    const INFO_ORDER_FUNCTION = 'Info_Order';
    const MAKE_ORDER_FUNCTION = 'Make_Order';
    const INFO_DOMAIN_FUNCTION = 'Info_Domain';
    const INFO_OBJECT_FUNCTION = 'Info_Object';
    const INFO_CONTACT_FUNCTION = 'Info_Contact';
    const GET_DNS_ZONE_FUNCTION = 'Get_DNS_Zone';
    const SET_DNS_ZONE_FUNCTION = 'Set_DNS_Zone';
    const GET_EMAIL_FORWARDING_FUNCTION = 'Get_Email_Forwarding';
    const SET_EMAIL_FORWARDING_FUNCTION = 'Set_Email_Forwarding';
    const WHMCS_SYNC_FUNCTION = 'WHMCS_Sync';
    const WHMCS_TRANSFER_SYNC_FUNCTION = 'WHMCS_TransferSync';

    /**
     * Available types of the function Make_Order
     */
    const SEND_AUTH_ID_MAKE_ORDER_TYPE = 'SendAuthID_Domain';
    const MODIFY_DOMAIN_MAKE_ORDER_TYPE = 'Modify_Domain';
    const UNLOCK_DOMAIN_MAKE_ORDER_TYPE = 'UnLock_Domain';
    const LOCK_DOMAIN_MAKE_ORDER_TYPE = 'Lock_Domain';
    const MODIFY_NS_DOMAIN_MAKE_ORDER_TYPE = 'ModifyNS_Domain';
    const RENEW_DOMAIN_MAKE_ORDER_TYPE = 'Renew_Domain';
    const CREATE_DOMAIN_MAKE_ORDER_TYPE = 'Create_Domain';
    const TRANSFER_DOMAIN_MAKE_ORDER_TYPE = 'Transfer_Domain';

    /**
     * Nsset object for function Info_Order
     */
    const INFO_OBJECT_NSSET = 'nsset';

    /**
     * SOAP function name
     *
     * @var string
     */
    private $function = null;

    /**
     * SOAP request data
     *
     * @var array
     */
    private $data = [];

    /**
     * Input WHMCS parameters
     *
     * @var array
     */
    private $params = [];

    /**
     * SoapRequest construktor
     *
     * @param string $function
     * @param array $data
     * @param array $params
     */
    public function __construct($function, $params = [], $data = [])
    {
        $this->function = $function;
        $this->params = $params;
        $this->data = $data;
    }


    /**
     * Sets SOAP reqest data
     *
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


    /**
     * Return SOAP reqest data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Sets input WHMCS parameters
     *
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }


    /**
     * Return input WHMCS parameters
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }


    /**
     * Sets SOAP function
     *
     * @param string $function
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }


    /**
     * Returns SOAP function
     *
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }
}