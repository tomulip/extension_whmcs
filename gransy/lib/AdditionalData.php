<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * Class for extending additional data for top level domain
 */
class AdditionalData
{
    /**
     * Text value constant
     */
    CONST TEXT_VALUE = 'text';

    /**
     * Tick value constant
     */
    CONST TICK_VALUE = 'tick';

    /**
     * Tick value ON constant
     */
    CONST TICK_VALUE_ON = 'on';

    /**
     * Tld additional data definition
     *
     * @var array
     */
    private $additionalData = array (
  'abogado' => 
  array (
    'law-accreditation-b' => 'text',
    'law-accreditation-id' => 'text',
    'law-accredita-year' => 'text',
    'law-jurisdiction-country' => 'text',
  ),
  'academy' => 
  array (
    'lang_info' => 'text',
  ),
  'accountants' => 
  array (
    'lang_info' => 'text',
  ),
  'actor' => 
  array (
    'lang_info' => 'text',
  ),
  'aero' => 
  array (
    'aero-id' => 'text',
    'aero-key' => 'text',
  ),
  'agency' => 
  array (
    'lang_info' => 'text',
  ),
  'airforce' => 
  array (
    'lang_info' => 'text',
  ),
  'com.al' => 
  array (
    'trustee' => 'tick',
  ),
  'apartments' => 
  array (
    'lang_info' => 'text',
  ),
  'com.ar' => 
  array (
    'trustee' => 'tick',
  ),
  'army' => 
  array (
    'lang_info' => 'text',
  ),
  'org.ar' => 
  array (
    'trustee' => 'tick',
  ),
  'art' => 
  array (
    'lang' => 'text',
  ),
  'associates' => 
  array (
    'lang_info' => 'text',
  ),
  'attorney' => 
  array (
    'lang_info' => 'text',
  ),
  'auction' => 
  array (
    'lang_info' => 'text',
  ),
  'auto' => 
  array (
    'lang_info' => 'text',
  ),
  'az' => 
  array (
    'idnum' => 'text',
  ),
  'ba' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'band' => 
  array (
    'lang_info' => 'text',
  ),
  'bargains' => 
  array (
    'lang_info' => 'text',
  ),
  'bayern' => 
  array (
    'trustee' => 'tick',
  ),
  'bb' => 
  array (
    'trustee' => 'tick',
  ),
  'be' => 
  array (
    'quarantine' => 'tick',
  ),
  'berlin' => 
  array (
    'trustee' => 'tick',
  ),
  'bg' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'bh' => 
  array (
    'trustee' => 'tick',
  ),
  'bike' => 
  array (
    'lang_info' => 'text',
  ),
  'bingo' => 
  array (
    'lang_info' => 'text',
  ),
  'biz' => 
  array (
    'lang_biz' => 'text',
  ),
  'boutique' => 
  array (
    'lang_info' => 'text',
  ),
  'com.br' => 
  array (
    'CNPJ' => 'text',
    'CPF' => 'text',
    'trustee' => 'tick',
  ),
  'net.br' => 
  array (
    'trustee' => 'tick',
  ),
  'builders' => 
  array (
    'lang_info' => 'text',
  ),
  'business' => 
  array (
    'lang_info' => 'text',
  ),
  'by' => 
  array (
    'idnum' => 'text',
    'issue_date' => 'text',
    'issue_org' => 'text',
    'passport' => 'text',
    'vat' => 'text',
  ),
  'ca' => 
  array (
    'legaltype' => 'text',
  ),
  'cab' => 
  array (
    'lang_info' => 'text',
  ),
  'cafe' => 
  array (
    'lang_info' => 'text',
  ),
  'cam' => 
  array (
    'lang_info' => 'text',
  ),
  'camera' => 
  array (
    'lang_info' => 'text',
  ),
  'camp' => 
  array (
    'lang_info' => 'text',
  ),
  'capital' => 
  array (
    'lang_info' => 'text',
  ),
  'car' => 
  array (
    'lang_info' => 'text',
  ),
  'cards' => 
  array (
    'lang_info' => 'text',
  ),
  'care' => 
  array (
    'lang_info' => 'text',
  ),
  'careers' => 
  array (
    'lang_info' => 'text',
  ),
  'cars' => 
  array (
    'lang_info' => 'text',
  ),
  'cash' => 
  array (
    'lang_info' => 'text',
  ),
  'casino' => 
  array (
    'lang_info' => 'text',
  ),
  'cat' => 
  array (
    'reason' => 'text',
  ),
  'catering' => 
  array (
    'lang_info' => 'text',
  ),
  'center' => 
  array (
    'lang_info' => 'text',
  ),
  'city' => 
  array (
    'lang_info' => 'text',
  ),
  'claims' => 
  array (
    'lang_info' => 'text',
  ),
  'cleaning' => 
  array (
    'lang_info' => 'text',
  ),
  'clinic' => 
  array (
    'lang_info' => 'text',
  ),
  'clothing' => 
  array (
    'lang_info' => 'text',
  ),
  'club' => 
  array (
    'lang_info' => 'text',
  ),
  'cn' => 
  array (
    'trustee' => 'tick',
  ),
  'com.cn' => 
  array (
    'trustee' => 'tick',
  ),
  'net.cn' => 
  array (
    'trustee' => 'tick',
  ),
  'org.cn' => 
  array (
    'trustee' => 'tick',
  ),
  'sh.cn' => 
  array (
    'trustee' => 'tick',
  ),
  'coach' => 
  array (
    'lang_info' => 'text',
  ),
  'codes' => 
  array (
    'lang_info' => 'text',
  ),
  'coffee' => 
  array (
    'lang_info' => 'text',
  ),
  'com' => 
  array (
    'lang' => 'text',
  ),
  'community' => 
  array (
    'lang_info' => 'text',
  ),
  'company' => 
  array (
    'lang_info' => 'text',
  ),
  'computer' => 
  array (
    'lang_info' => 'text',
  ),
  'condos' => 
  array (
    'lang_info' => 'text',
  ),
  'construction' => 
  array (
    'lang_info' => 'text',
  ),
  'consulting' => 
  array (
    'lang_info' => 'text',
  ),
  'contractors' => 
  array (
    'lang_info' => 'text',
  ),
  'cool' => 
  array (
    'lang_info' => 'text',
  ),
  'coupons' => 
  array (
    'lang_info' => 'text',
  ),
  'cr' => 
  array (
    'idnum' => 'text',
  ),
  'credit' => 
  array (
    'lang_info' => 'text',
  ),
  'creditcard' => 
  array (
    'lang_info' => 'text',
  ),
  'cruises' => 
  array (
    'lang_info' => 'text',
  ),
  'com.cy' => 
  array (
    'trustee' => 'tick',
  ),
  'dance' => 
  array (
    'lang_info' => 'text',
  ),
  'dating' => 
  array (
    'lang_info' => 'text',
  ),
  'de' => 
  array (
    'trustee' => 'tick',
  ),
  'deals' => 
  array (
    'lang_info' => 'text',
  ),
  'degree' => 
  array (
    'lang_info' => 'text',
  ),
  'delivery' => 
  array (
    'lang_info' => 'text',
  ),
  'democrat' => 
  array (
    'lang_info' => 'text',
  ),
  'dental' => 
  array (
    'lang_info' => 'text',
  ),
  'dentist' => 
  array (
    'lang_info' => 'text',
  ),
  'diamonds' => 
  array (
    'lang_info' => 'text',
  ),
  'digital' => 
  array (
    'lang_info' => 'text',
  ),
  'direct' => 
  array (
    'lang_info' => 'text',
  ),
  'directory' => 
  array (
    'lang_info' => 'text',
  ),
  'discount' => 
  array (
    'lang_info' => 'text',
  ),
  'dk' => 
  array (
    'idnum' => 'text',
  ),
  'doctor' => 
  array (
    'lang_info' => 'text',
  ),
  'dog' => 
  array (
    'lang_info' => 'text',
  ),
  'domains' => 
  array (
    'lang_info' => 'text',
  ),
  'education' => 
  array (
    'lang_info' => 'text',
  ),
  'ee' => 
  array (
    'authid' => 'text',
    'idnum' => 'text',
    'idnum_type' => 'text',
    'trustee' => 'tick',
  ),
  'com.ee' => 
  array (
    'trustee' => 'tick',
  ),
  'email' => 
  array (
    'lang_info' => 'text',
  ),
  'energy' => 
  array (
    'lang_info' => 'text',
  ),
  'engineer' => 
  array (
    'lang_info' => 'text',
  ),
  'engineering' => 
  array (
    'lang_info' => 'text',
  ),
  'enterprises' => 
  array (
    'lang_info' => 'text',
  ),
  'equipment' => 
  array (
    'lang_info' => 'text',
  ),
  'es' => 
  array (
    'ES-NIF-NIE' => 'text',
    'IDCARD-OR-PASSPORT-NUMBER' => 'text',
  ),
  'estate' => 
  array (
    'lang_info' => 'text',
  ),
  'eu' => 
  array (
    'lang_eu' => 'text',
  ),
  'events' => 
  array (
    'lang_info' => 'text',
  ),
  'exchange' => 
  array (
    'lang_info' => 'text',
  ),
  'expert' => 
  array (
    'lang_info' => 'text',
  ),
  'exposed' => 
  array (
    'lang_info' => 'text',
  ),
  'express' => 
  array (
    'lang_info' => 'text',
  ),
  'fail' => 
  array (
    'lang_info' => 'text',
  ),
  'family' => 
  array (
    'lang_info' => 'text',
  ),
  'farm' => 
  array (
    'lang_info' => 'text',
  ),
  'fi' => 
  array (
    'identity' => 'text',
    'type' => 'text',
  ),
  'finance' => 
  array (
    'lang_info' => 'text',
  ),
  'financial' => 
  array (
    'lang_info' => 'text',
  ),
  'fish' => 
  array (
    'lang_info' => 'text',
  ),
  'fitness' => 
  array (
    'lang_info' => 'text',
  ),
  'flights' => 
  array (
    'lang_info' => 'text',
  ),
  'florist' => 
  array (
    'lang_info' => 'text',
  ),
  'football' => 
  array (
    'lang_info' => 'text',
  ),
  'forsale' => 
  array (
    'lang_info' => 'text',
  ),
  'foundation' => 
  array (
    'lang_info' => 'text',
  ),
  'fr' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'fun' => 
  array (
    'lang' => 'text',
  ),
  'fund' => 
  array (
    'lang_info' => 'text',
  ),
  'furniture' => 
  array (
    'lang_info' => 'text',
  ),
  'futbol' => 
  array (
    'lang_info' => 'text',
  ),
  'fyi' => 
  array (
    'lang_info' => 'text',
  ),
  'gallery' => 
  array (
    'lang_info' => 'text',
  ),
  'games' => 
  array (
    'lang_info' => 'text',
  ),
  'ge' => 
  array (
    'trustee' => 'tick',
  ),
  'moscow' => 
  array (
    'contact_type' => 'text',
    'date_of_birth' => 'text',
    'idnum' => 'text',
    'tin' => 'text',
  ),
  'xn--80adxhks' => 
  array (
    'contact_type' => 'text',
    'date_of_birth' => 'text',
    'idnum' => 'text',
    'tin' => 'text',
  ),
  'gal' => 
  array (
    'X-INTENDED-USE' => 'text',
  ),
  'gifts' => 
  array (
    'lang_info' => 'text',
  ),
  'gives' => 
  array (
    'lang_info' => 'text',
  ),
  'glass' => 
  array (
    'lang_info' => 'text',
  ),
  'global' => 
  array (
    'lang_info' => 'text',
  ),
  'gmbh' => 
  array (
    'lang_info' => 'text',
  ),
  'gold' => 
  array (
    'lang_info' => 'text',
  ),
  'golf' => 
  array (
    'lang_info' => 'text',
  ),
  'graphics' => 
  array (
    'lang_info' => 'text',
  ),
  'gratis' => 
  array (
    'lang_info' => 'text',
  ),
  'gripe' => 
  array (
    'lang_info' => 'text',
  ),
  'group' => 
  array (
    'lang_info' => 'text',
  ),
  'guide' => 
  array (
    'lang_info' => 'text',
  ),
  'guru' => 
  array (
    'lang_info' => 'text',
  ),
  'hamburg' => 
  array (
    'trustee' => 'tick',
  ),
  'haus' => 
  array (
    'lang_info' => 'text',
  ),
  'healthcare' => 
  array (
    'lang_info' => 'text',
  ),
  'hk' => 
  array (
    'cttype' => 'text',
    '1_cttype' => 'text',
    '2_docTypeI' => 'text',
    '2_docTypeO' => 'text',
    '3_otherDoc' => 'text',
    '4_docNum' => 'text',
    '5_docOriginCC' => 'text',
    '6_industryType' => 'text',
    '7_under18' => 'tick',
  ),
  'com.hk' => 
  array (
    'cttype' => 'text',
    '1_cttype' => 'text',
    '2_docTypeI' => 'text',
    '2_docTypeO' => 'text',
    '3_otherDoc' => 'text',
    '4_docNum' => 'text',
    '5_docOriginCC' => 'text',
    '6_industryType' => 'text',
    '7_under18' => 'tick',
    'trustee' => 'tick',
  ),
  'hockey' => 
  array (
    'lang_info' => 'text',
  ),
  'holdings' => 
  array (
    'lang_info' => 'text',
  ),
  'holiday' => 
  array (
    'lang_info' => 'text',
  ),
  'hospital' => 
  array (
    'lang_info' => 'text',
  ),
  'host' => 
  array (
    'lang' => 'text',
  ),
  'house' => 
  array (
    'lang_info' => 'text',
  ),
  'hr' => 
  array (
    'idnum' => 'text',
  ),
  'com.hr' => 
  array (
    'idnum' => 'text',
  ),
  'hu' => 
  array (
    'dnssec' => 'tick',
    'idnum' => 'text',
    'statehu' => 'text',
    'trustee' => 'tick',
  ),
  'bolt.hu' => 
  array (
    'idnum' => 'text',
    'trustee_owner' => 'text',
    'trustee' => 'tick',
  ),
  'co.hu' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'chat' => 
  array (
    'lang_info' => 'text',
  ),
  'cheap' => 
  array (
    'lang_info' => 'text',
  ),
  'church' => 
  array (
    'lang_info' => 'text',
  ),
  'id' => 
  array (
    'pouzitiDomeny' => 'text',
    'trustee' => 'tick',
  ),
  'co.id' => 
  array (
    'pouzitiDomeny' => 'text',
    'trustee' => 'tick',
  ),
  'ie' => 
  array (
    'holder_class' => 'text',
    'idnum' => 'text',
    'remarks' => 'text',
  ),
  'immo' => 
  array (
    'lang_info' => 'text',
  ),
  'immobilien' => 
  array (
    'lang_info' => 'text',
  ),
  'industries' => 
  array (
    'lang_info' => 'text',
  ),
  'info' => 
  array (
    'lang_info' => 'text',
  ),
  'institute' => 
  array (
    'lang_info' => 'text',
  ),
  'insure' => 
  array (
    'lang_info' => 'text',
  ),
  'international' => 
  array (
    'lang_info' => 'text',
  ),
  'investments' => 
  array (
    'lang_info' => 'text',
  ),
  'iq' => 
  array (
    'trustee' => 'tick',
  ),
  'com.iq' => 
  array (
    'trustee' => 'tick',
  ),
  'ir' => 
  array (
    'authority' => 'text',
    'IDCARD-OR-PASSPORT-ISSUER' => 'text',
    'IDCARD-OR-PASSPORT-NUMBER' => 'text',
    'idnum' => 'text',
    'IR-COMPANY-REGISTRATION-CC' => 'text',
    'IR-COMPANY-REGISTRATION-SP' => 'text',
    'legaltype' => 'text',
  ),
  'co.ir' => 
  array (
    'authority' => 'text',
    'IDCARD-OR-PASSPORT-ISSUER' => 'text',
    'IDCARD-OR-PASSPORT-NUMBER' => 'text',
    'idnum' => 'text',
    'IR-COMPANY-REGISTRATION-CC' => 'text',
    'IR-COMPANY-REGISTRATION-SP' => 'text',
    'legaltype' => 'text',
  ),
  'it' => 
  array (
    'entityType' => 'text',
    'nationalityCode' => 'text',
    'regCode' => 'text',
  ),
  'jetzt' => 
  array (
    'lang_info' => 'text',
  ),
  'jewelry' => 
  array (
    'lang_info' => 'text',
  ),
  'jo' => 
  array (
    'trustee' => 'tick',
  ),
  'jobs' => 
  array (
    'X-JOBS-COMPANYURL' => 'text',
  ),
  'com.jo' => 
  array (
    'trustee' => 'tick',
  ),
  'kaufen' => 
  array (
    'lang_info' => 'text',
  ),
  'kitchen' => 
  array (
    'lang_info' => 'text',
  ),
  'kr' => 
  array (
    'trustee' => 'tick',
  ),
  'co.kr' => 
  array (
    'trustee' => 'tick',
  ),
  'krd' => 
  array (
    'law-accreditation-b' => 'text',
    'law-accreditation-id' => 'text',
    'law-accredita-year' => 'text',
    'law-jurisdiction-country' => 'text',
  ),
  'com.kw' => 
  array (
    'trustee' => 'tick',
  ),
  'land' => 
  array (
    'lang_info' => 'text',
  ),
  'law' => 
  array (
    'law-accreditation-b' => 'text',
    'law-accreditation-id' => 'text',
    'law-accredita-year' => 'text',
    'law-jurisdiction-country' => 'text',
  ),
  'lawyer' => 
  array (
    'lang_info' => 'text',
  ),
  'com.lb' => 
  array (
    'trustee' => 'tick',
  ),
  'net.lb' => 
  array (
    'trustee' => 'tick',
  ),
  'org.lb' => 
  array (
    'trustee' => 'tick',
  ),
  'lease' => 
  array (
    'lang_info' => 'text',
  ),
  'legal' => 
  array (
    'lang_info' => 'text',
  ),
  'life' => 
  array (
    'lang_info' => 'text',
  ),
  'lighting' => 
  array (
    'lang_info' => 'text',
  ),
  'limited' => 
  array (
    'lang_info' => 'text',
  ),
  'limo' => 
  array (
    'lang_info' => 'text',
  ),
  'link' => 
  array (
    'lang_info' => 'text',
  ),
  'live' => 
  array (
    'lang_info' => 'text',
  ),
  'loans' => 
  array (
    'lang_info' => 'text',
  ),
  'lotto' => 
  array (
    'license_number' => 'text',
  ),
  'ltd' => 
  array (
    'lang_info' => 'text',
  ),
  'ma' => 
  array (
    'trustee' => 'tick',
  ),
  'co.ma' => 
  array (
    'trustee' => 'tick',
  ),
  'maison' => 
  array (
    'lang_info' => 'text',
  ),
  'management' => 
  array (
    'lang_info' => 'text',
  ),
  'market' => 
  array (
    'lang_info' => 'text',
  ),
  'marketing' => 
  array (
    'lang_info' => 'text',
  ),
  'mba' => 
  array (
    'lang_info' => 'text',
  ),
  'media' => 
  array (
    'lang_info' => 'text',
  ),
  'memorial' => 
  array (
    'lang_info' => 'text',
  ),
  'mk' => 
  array (
    'idnum' => 'text',
  ),
  'com.mk' => 
  array (
    'trustee' => 'tick',
  ),
  'moda' => 
  array (
    'lang_info' => 'text',
  ),
  'money' => 
  array (
    'lang_info' => 'text',
  ),
  'mortgage' => 
  array (
    'lang_info' => 'text',
  ),
  'movie' => 
  array (
    'lang_info' => 'text',
  ),
  'my' => 
  array (
    'trustee' => 'tick',
  ),
  'com.my' => 
  array (
    'trustee' => 'tick',
  ),
  'navy' => 
  array (
    'lang_info' => 'text',
  ),
  'net' => 
  array (
    'lang' => 'text',
  ),
  'network' => 
  array (
    'lang_info' => 'text',
  ),
  'news' => 
  array (
    'lang_info' => 'text',
  ),
  'ninja' => 
  array (
    'lang_info' => 'text',
  ),
  'no' => 
  array (
    'trustee' => 'tick',
  ),
  'nu' => 
  array (
    'admin_idnum' => 'text',
    'admin_vat' => 'text',
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'observer' => 
  array (
    'lang' => 'text',
  ),
  'co.om' => 
  array (
    'trustee' => 'tick',
  ),
  'com.om' => 
  array (
    'trustee' => 'tick',
  ),
  'online' => 
  array (
    'lang' => 'text',
  ),
  'org' => 
  array (
    'lang_info' => 'text',
  ),
  'partners' => 
  array (
    'lang_info' => 'text',
  ),
  'parts' => 
  array (
    'lang_info' => 'text',
  ),
  'ph' => 
  array (
    'Registrant' => 'text',
  ),
  'photography' => 
  array (
    'lang_info' => 'text',
  ),
  'photos' => 
  array (
    'lang_info' => 'text',
  ),
  'pictures' => 
  array (
    'lang_info' => 'text',
  ),
  'pizza' => 
  array (
    'lang_info' => 'text',
  ),
  'place' => 
  array (
    'lang_info' => 'text',
  ),
  'plumbing' => 
  array (
    'lang_info' => 'text',
  ),
  'plus' => 
  array (
    'lang_info' => 'text',
  ),
  'pm' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'porn' => 
  array (
    'xxx-defense' => 'text',
    'xxx-memberid' => 'text',
    'xxx-password' => 'text',
  ),
  'press' => 
  array (
    'lang' => 'text',
  ),
  'productions' => 
  array (
    'lang_info' => 'text',
  ),
  'properties' => 
  array (
    'lang_info' => 'text',
  ),
  'pt' => 
  array (
    'idnum' => 'text',
  ),
  'com.pt' => 
  array (
    'idnum' => 'text',
  ),
  'pub' => 
  array (
    'lang_info' => 'text',
  ),
  're' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'realty' => 
  array (
    'lang' => 'text',
  ),
  'recipes' => 
  array (
    'lang_info' => 'text',
  ),
  'rehab' => 
  array (
    'lang_info' => 'text',
  ),
  'reisen' => 
  array (
    'lang_info' => 'text',
  ),
  'rentals' => 
  array (
    'lang_info' => 'text',
  ),
  'repair' => 
  array (
    'lang_info' => 'text',
  ),
  'report' => 
  array (
    'lang_info' => 'text',
  ),
  'republican' => 
  array (
    'lang_info' => 'text',
  ),
  'restaurant' => 
  array (
    'lang_info' => 'text',
  ),
  'reviews' => 
  array (
    'lang_info' => 'text',
  ),
  'rio' => 
  array (
    'X-BR-REGISTER-NUMBER' => 'text',
  ),
  'rip' => 
  array (
    'lang_info' => 'text',
  ),
  'ro' => 
  array (
    'idnumber' => 'text',
  ),
  'rocks' => 
  array (
    'lang_info' => 'text',
  ),
  'com.ro' => 
  array (
    'idnumber' => 'text',
  ),
  'rs' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'co.rs' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'edu.rs' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'in.rs' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'org.rs' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'ru' => 
  array (
    'birthdate' => 'text',
    'idnum' => 'text',
  ),
  'ruhr' => 
  array (
    'trustee' => 'tick',
  ),
  'run' => 
  array (
    'lang_info' => 'text',
  ),
  'spb.ru' => 
  array (
    'birthdate' => 'text',
    'idnum' => 'text',
  ),
  'sale' => 
  array (
    'lang_info' => 'text',
  ),
  'salon' => 
  array (
    'lang_info' => 'text',
  ),
  'sarl' => 
  array (
    'lang_info' => 'text',
  ),
  'scot' => 
  array (
    'x-intended-use' => 'text',
  ),
  'se' => 
  array (
    'admin_idnum' => 'text',
    'admin_vat' => 'text',
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'services' => 
  array (
    'lang_info' => 'text',
  ),
  'sg' => 
  array (
    'trustee' => 'tick',
  ),
  'com.sg' => 
  array (
    'trustee' => 'tick',
  ),
  'shoes' => 
  array (
    'lang_info' => 'text',
  ),
  'shop' => 
  array (
    'lang' => 'text',
  ),
  'shopping' => 
  array (
    'lang_info' => 'text',
  ),
  'show' => 
  array (
    'lang_info' => 'text',
  ),
  'school' => 
  array (
    'lang_info' => 'text',
  ),
  'schule' => 
  array (
    'lang_info' => 'text',
  ),
  'singles' => 
  array (
    'lang_info' => 'text',
  ),
  'site' => 
  array (
    'lang' => 'text',
  ),
  'sk' => 
  array (
    'sknicid' => 'text',
    'trustee' => 'tick',
  ),
  'soccer' => 
  array (
    'lang_info' => 'text',
  ),
  'social' => 
  array (
    'lang_info' => 'text',
  ),
  'software' => 
  array (
    'lang_info' => 'text',
  ),
  'solar' => 
  array (
    'lang_info' => 'text',
  ),
  'solutions' => 
  array (
    'lang_info' => 'text',
  ),
  'space' => 
  array (
    'lang' => 'text',
  ),
  'store' => 
  array (
    'lang' => 'text',
  ),
  'studio' => 
  array (
    'lang_info' => 'text',
  ),
  'style' => 
  array (
    'lang_info' => 'text',
  ),
  'su' => 
  array (
    'birthdate' => 'text',
    'idnum' => 'text',
  ),
  'supplies' => 
  array (
    'lang_info' => 'text',
  ),
  'supply' => 
  array (
    'lang_info' => 'text',
  ),
  'support' => 
  array (
    'lang_info' => 'text',
  ),
  'surgery' => 
  array (
    'lang_info' => 'text',
  ),
  'systems' => 
  array (
    'lang_info' => 'text',
  ),
  'tax' => 
  array (
    'lang_info' => 'text',
  ),
  'taxi' => 
  array (
    'lang_info' => 'text',
  ),
  'team' => 
  array (
    'lang_info' => 'text',
  ),
  'tech' => 
  array (
    'lang' => 'text',
  ),
  'technology' => 
  array (
    'lang_info' => 'text',
  ),
  'tennis' => 
  array (
    'lang_info' => 'text',
  ),
  'tf' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'theater' => 
  array (
    'lang_info' => 'text',
  ),
  'in.th' => 
  array (
    'trustee' => 'tick',
  ),
  'tienda' => 
  array (
    'lang_info' => 'text',
  ),
  'tips' => 
  array (
    'lang_info' => 'text',
  ),
  'tires' => 
  array (
    'lang_info' => 'text',
  ),
  'today' => 
  array (
    'lang_info' => 'text',
  ),
  'tools' => 
  array (
    'lang_info' => 'text',
  ),
  'top' => 
  array (
    'lang' => 'text',
  ),
  'tours' => 
  array (
    'lang_info' => 'text',
  ),
  'town' => 
  array (
    'lang_info' => 'text',
  ),
  'toys' => 
  array (
    'lang_info' => 'text',
  ),
  'training' => 
  array (
    'lang_info' => 'text',
  ),
  'travel' => 
  array (
    'uin' => 'text',
  ),
  'ua' => 
  array (
    'trademark' => 'text',
  ),
  'uk' => 
  array (
    'co-no' => 'text',
    'uktype' => 'text',
    'trustee' => 'tick',
  ),
  'co.uk' => 
  array (
    'co-no' => 'text',
    'uktype' => 'text',
  ),
  'me.uk' => 
  array (
    'co-no' => 'text',
    'uktype' => 'text',
  ),
  'org.uk' => 
  array (
    'co-no' => 'text',
    'uktype' => 'text',
  ),
  'university' => 
  array (
    'lang_info' => 'text',
  ),
  'us' => 
  array (
    'countrycode' => 'text',
    'us-nexus-apppurpose' => 'text',
    'us-nexus-category' => 'text',
  ),
  'vacations' => 
  array (
    'lang_info' => 'text',
  ),
  'ventures' => 
  array (
    'lang_info' => 'text',
  ),
  'vet' => 
  array (
    'lang_info' => 'text',
  ),
  'viajes' => 
  array (
    'lang_info' => 'text',
  ),
  'video' => 
  array (
    'lang_info' => 'text',
  ),
  'villas' => 
  array (
    'lang_info' => 'text',
  ),
  'vin' => 
  array (
    'lang_info' => 'text',
  ),
  'vision' => 
  array (
    'lang_info' => 'text',
  ),
  'voyage' => 
  array (
    'lang_info' => 'text',
  ),
  'watch' => 
  array (
    'lang_info' => 'text',
  ),
  'website' => 
  array (
    'lang' => 'text',
  ),
  'wf' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'wine' => 
  array (
    'lang_info' => 'text',
  ),
  'works' => 
  array (
    'lang_info' => 'text',
  ),
  'world' => 
  array (
    'lang_info' => 'text',
  ),
  'wtf' => 
  array (
    'lang_info' => 'text',
  ),
  'xn--e1a4c' => 
  array (
    'lang_eu' => 'text',
  ),
  'xn--p1ai' => 
  array (
    'birthdate' => 'text',
    'idnum' => 'text',
  ),
  'xn--vhquv' => 
  array (
    'lang_info' => 'text',
  ),
  'xn--90a3ac' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'xn--c1avg.xn--90a3ac' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'xn--d1at.xn--90a3ac' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'xn--o1ac.xn--90a3ac' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'xn--90azh.xn--90a3ac' => 
  array (
    'idnum' => 'text',
    'vat' => 'text',
  ),
  'xxx' => 
  array (
    'xxx-defense' => 'text',
    'xxx-memberid' => 'text',
    'xxx-password' => 'text',
  ),
  'xyz' => 
  array (
    'lang' => 'text',
  ),
  'yt' => 
  array (
    'idnum' => 'text',
    'trustee' => 'tick',
  ),
  'zone' => 
  array (
    'lang_info' => 'text',
  ),
);

    /**
     * Top level domain
     *
     * @var string
     */
    private $tld = null;

    /**
     * Additional data
     * @var array
     */
    private $data = [];


    /**
     * AdditionalData constructor
     *
     * @param string $tld
     * @param array $fields
     */
    public function __construct($tld, $fields = [])
    {
        $this->tld = $tld;

        $this->setData($fields);
    }


    /**
     * Sets additional data for SOAP request
     *
     * @param array $fields
     */
    public function setData($fields)
    {
        if (isset($this->additionalData[$this->tld]))
        {
            $definition = $this->additionalData[$this->tld];

            foreach ($fields as $key => $value)
            {
                switch ($definition[$key])
                {
                    case self::TICK_VALUE:
                           $this->data[$key] = $value == self::TICK_VALUE_ON ? 1 : trim($value);

                        break;

                    case self::TEXT_VALUE:
                        $this->data[$key] = trim($value);

                        break;

                    default:
                        break;
                }
            }
        }
    }


    /**
     * Returns additionals data for SOAP request
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }


    /**
     * Returns true for existing additional data
     *
     * @return bool
     */
    public function hasData()
    {
        return !empty($this->data);
    }
}