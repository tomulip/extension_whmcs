<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * Class of exception throws on SOAP failed response (status = error)
 */
class SoapResponseException extends \Exception
{
    /**
     * Flag for non-existing object
     *
     * @var bool
     */
    public $notExists = false;
}