<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * General SOAP response class
 */
class SoapResponse
{
    /**
     * Major and minor response codes for non-existing object
     */
    const OBJECT_NOT_EXISTS_MAJOR_CODE = 501;
    const OBJECT_NOT_EXISTS_MINOR_CODE = 1004;

    /**
     * Response states
     */
    const SUCCESS_STATUS = 'ok';
    const ERROR_STATUS = 'error';

    /**
     * Stavy objednavky
     */
    const ORDER_STATUS_COMPLETED = 'Completed';

    /**
     * Response state
     *
     * @var null
     */
    private $status = null;

    /**
     * Response data
     *
     * @var array
     */
    private $data = [];


    /**
     * SoapResponse konstruktor
     *
     * @param array $response
     */
    public function __construct($response)
    {
        $this->status = $response['status'];

        if ($this->isSuccess())
        {
            $this->data = $response['data'];
        }
        else
        {
            $this->data = $response['error'];
        }
    }


    /**
     * Returns sucess response flag
     *
     * @return bool
     */
    public function isSuccess()
    {
        return $this->status === self::SUCCESS_STATUS;
    }


    /**
     * Returns failed response flag
     *
     * @return bool
     */
    public function isError()
    {
        return $this->status === self::ERROR_STATUS;
    }


    /**
     * Returns error message
     *
     * @return string|null
     */
    public function getErrorMessage()
    {
        if ($this->isError())
        {
            return $this->data['errormsg'];
        }

        return null;
    }


    /**
     * Returns major error code
     *
     * @return int|null
     */
    public function getErrorMajorCode()
    {
        if ($this->isError())
        {
            return $this->data['errorcode']['major'];
        }

        return null;
    }


    /**
     * Returns minor error code
     *
     * @return int|null
     */
    public function getErrorMinorCode()
    {
        if ($this->isError())
        {
            return $this->data['errorcode']['minor'];
        }

        return null;
    }


    /**
     * Returns flag of non-existing object
     *
     * @return bool
     */
    public function objectNotExists()
    {
        return $this->getErrorMajorCode() == self::OBJECT_NOT_EXISTS_MAJOR_CODE &&
                    $this->getErrorMinorCode() == self::OBJECT_NOT_EXISTS_MINOR_CODE;
    }


    /**
     * Returns response data
     *
     * @param string $key
     * @return array|string|int
     */
    public function getData($key = null)
    {
        if ($key !== null)
        {
            return isset($this->data[$key]) ? $this->data[$key] : null;
        }

        return $this->data;
    }
}