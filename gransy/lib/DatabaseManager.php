<?php

namespace WHMCS\Module\Registrar\Gransy;

use WHMCS\Database\Capsule;

/**
 * Class for communication with database table orders
 */
class DatabaseManager
{
    /**
     * Table name for orders
     */
    const TABLE_ORDERS_NAME = 'mod_gransy_orders';

    /**
     * Returns PDO connection to database
     *
     * @return \PDO
     */
    private static function getPDOConnection()
    {
        return Capsule::connection()->getPdo();
    }


    /**
     * Checks whether the orders table exists and, if not, creates it
     *
     * @param bool $enableLogging
     *
     * @return void
     **/
    public static function createOrdersTable($enableLogging = false)
    {
        if (Capsule::schema()->hasTable(self::TABLE_ORDERS_NAME) === false)
        {
            try
            {
                $pdo = self::getPDOConnection();

                $sql = 'CREATE TABLE IF NOT EXISTS `' . self::TABLE_ORDERS_NAME . '` (
	                      `id` INT(11) NOT NULL,
	                      `domain` varchar(200) NOT NULL,
	                    PRIMARY KEY (`id`),
	                    KEY `domain` (`domain`)
	                    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

                $pdo->exec($sql);
            }
            catch (\PDOException $e)
            {
                if ($enableLogging)
                {
                    Logger::log(get_called_class() . '::createOrdersTable', null, $e->getMessage());
                }
            }
        }
    }


    /**
     * Insert the current order (orderid, domain) into the orders table
     *
     * @param string $domain
     * @param int $orderId
     * @param bool $enableLogging
     */
    public static function insertOrder($domain, $orderId, $enableLogging = false)
    {
        try
        {
            if ($orderId && $domain)
            {
                $pdo = self::getPDOConnection();

                $stmt = $pdo->prepare('
                    DELETE FROM 
                        `' . self::TABLE_ORDERS_NAME . '` 
                    WHERE 
                      `domain` = :name');

                $stmt->bindParam(':name', $domain, \PDO::PARAM_STR);
                $stmt->execute();

                $stmt = $pdo->prepare('
                  INSERT INTO 
                      `' . self::TABLE_ORDERS_NAME . '` (`id`, `domain`) 
                  VALUES
                      (:id, :name)');

                $stmt->bindParam(':id', $orderId, \PDO::PARAM_INT);
                $stmt->bindParam(':name', $domain, \PDO::PARAM_STR);
                $stmt->execute();
            }
        }
        catch (\PDOException $e)
        {
            if ($enableLogging)
            {
                Logger::log(get_called_class() . '::insertOrder', null, $e->getMessage());
            }
        }
    }


    /**
     * Returns the order details (orderid) by domain name
     *
     * @param string $domain
     * @param bool $enableLogging
     *
     * @return array
     */
    public static function getOrderByDomain($domain, $enableLogging = false)
    {
        $result = [];

        $pdo = self::getPDOConnection();

        try
        {
            $stmt = $pdo->prepare('
              SELECT 
                * 
              FROM 
                `' . self::TABLE_ORDERS_NAME . '`
              WHERE 
                `domain` = :name');

            $stmt->execute(array(':name' => $domain));

            $result =  $stmt->fetch(\PDO::FETCH_ASSOC);
        }
        catch (\PDOException $e)
        {
            if ($enableLogging)
            {
                Logger::log(get_called_class() . '::getOrderByDomain', null, $e->getMessage());
            }
        }

        return $result;
    }
}