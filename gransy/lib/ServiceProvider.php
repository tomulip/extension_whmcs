<?php

namespace WHMCS\Module\Registrar\Gransy;

use WHMCS\Domains\DomainLookup\SearchResult;
use WHMCS\Domains\DomainLookup\ResultsList;

/**
 * Base module class for domains operations
 */
class ServiceProvider
{
    /**
     * Types list of SOAP request Make_order
     * @var array
     */
    static private $orderTypes = [
        'Create_Domain' => 'Domain registration',
        'Transfer_Domain' => 'Domain transfer',
        'Renew_Domain' => 'Domain renew',
        'Modify_Domain' => 'Domain modification',
        'ModifyNS_Domain' => 'Nameserver change',
        'Lock_Domain' => 'Locking domain',
        'UnLock_Domain' => 'Unlocking domain',
        'SendAuthID_Domain' => 'Sending AuthID'
    ];

    /**
     * Cache list of loaded responses of domain detail
     *
     * @var SoapResponse[]
     */
    static private $domainInfo = [];


    /**
     * Init registrator module
     */
    public static function init()
    {
        if (phpversion() < 5.6)
        {
            throw new \Exception('PHP versions lower than 5.6 are not supported');
        }

        if (!extension_loaded('pdo_mysql'))
        {
            throw new \Exception('PHP pdo_mysql extension is required, but not installed');
        }

        if (!extension_loaded('soap'))
        {
            throw new \Exception('PHP soap extension is required, but not installed');
        }

        DatabaseManager::createOrdersTable();
    }


    /**
     * Send request to SOAP server and return response
     *
     * @param SoapRequest $request
     *
     * @return SoapResponse
     */
    public static function sendRequest($request)
    {
        return SoapClient::getConnection($request->getParams())->call($request->getFunction(), $request->getData());
    }


    /**
     * Inserts new record for processed domain (orderid, domainname)
     *
     * @param string $domain
     * @param int $orderId
     */
    public static function createOrder($domain, $orderId)
    {
        DatabaseManager::insertOrder($domain, $orderId);
    }


    /**
     * Determine if a domain or group of domains are available for
     * registration or transfer.
     *
     * @param array $params
     *
     * @return \WHMCS\Domains\DomainLookup\ResultsList
     */
    public static function checkDomainAvailability($params)
    {
        $resultList = new ResultsList();

        $suggestMaxResultCount = 0;

        foreach ($params['tldsToInclude'] as $tld)
        {
            if ($suggestMaxResultCount++ > $params['suggestMaxResultCount'])
            {
                break;
            }

            try
            {
                $domain = $params['isIdnDomain'] ? $params['punyCodeSearchTerm'] : $params['searchTerm'];
                $tld = substr($tld, 0, 1) === '.' ? $tld : '.' . $tld;

                $request = new SoapRequest(
                    SoapRequest::CHECK_DOMAIN_FUNCTION,
                    $params,
                    [
                        'data' => [
                            'domain' => $domain . $tld
                        ]
                    ]
                );

                $response = ServiceProvider::sendRequest($request);

                $searchResult = new SearchResult($params['searchTerm'], $tld);

                if ($response->getData('avail') == 1)
                {
                    $status = SearchResult::STATUS_NOT_REGISTERED;
                }
                else
                {
                    $status = SearchResult::STATUS_REGISTERED;
                }

                $searchResult->setStatus($status);

                $price = $response->getData('price');

                if ($price['premium'] == 1 && $params['premiumEnabled'])
                {
                    $searchResult->setPremiumDomain(true);

                    $searchResult->setPremiumCostPricing(
                        [
                            'register'     => $price['amount'],
                            'renew'        => $price['amount'],
                            'CurrencyCode' => $price['currency'],
                        ]
                    );
                }

                $resultList->append($searchResult);
            }
            catch (\Exception $e)
            {
            }
        }

        return $resultList;
    }


    /**
     * Returns html code of order error
     *
     * @param $params
     * @param $error
     *
     * @return string
     */
    public static function getOrderErrorHtmlCode($params, $error)
    {
        $domain = DatabaseManager::getOrderByDomain($params['sld'] . '.' . $params['tld']);

        $html = '';

        if ($domain)
        {
            try
            {
                $request = new SoapRequest(
                    SoapRequest::INFO_ORDER_FUNCTION,
                    $params,
                    [
                        'data' => [
                            'order' => $domain['id']
                        ]
                    ]
                );

                $response = self::sendRequest($request);

                $order = $response->getData('order');

                if ($order['status'] != SoapResponse::ORDER_STATUS_COMPLETED)
                {
                    if ($error)
                    {
                        $html .= '<br /><br />';
                    }

                    $html .= '<strong style=\'color: red;\'>order status: ' . $order['status'] . '</strong><br />';

                    if (isset(self::$orderTypes[$order['type']]))
                    {
                        $html .= 'Operation: <strong>' . self::$orderTypes[$order['type']] . '</strong><br />';
                    }

                    if (strpos($_SERVER['SCRIPT_NAME'], '/admin/') !== false &&
                        strpos($_SERVER['SCRIPT_NAME'], 'clientarea') === false)
                    {
                        $html .= 'Type: <strong>' . $order['type'] . ' (' . $order['id'] . ')</strong><br />';
                        $html .= 'Amount: <strong>' . $order['amount'] . '</strong><br />';
                    }

                    if (!empty($order['message']))
                    {
                        $html .= 'Message: <strong>' . $order['message'] . '</strong><br />';
                    }
                }
            }
            catch (\Exception $e)
            {
            }
        }

        return $html;
    }
    
    
    /**
     * Returns domain info from server
     *
     * @param $params
     *
     * @return SoapResponse
     */
    public static function getDomainInfo($params)
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        if (!self::$domainInfo[$domain])
        {
            $request = new SoapRequest(
                SoapRequest::INFO_DOMAIN_FUNCTION,
                $params,
                [
                    'data' => [
                        'domain' => $domain
                    ]
                ]
            );

            self::$domainInfo[$domain] = self::sendRequest($request);
        }

        return self::$domainInfo[$domain];
    }


    /**
     * Returns domain name for registration (transfer)
     *
     * @param array $params
     *
     * @return string
     *
     */
    public static function getDomainNameForRegister($params)
    {
        if ($params['original']['sld'] != '' && $params['original']['tld'] != '')
        {
            $domain = $params['original']['sld'] . '.' . $params['original']['tld'];
        }
        else
        {
            $domain = $params['sld'] . '.' . $params['tld'];
        }

        return $domain;
    }


    /**
     * Returns domain name for registration (transfer)
     *
     * @param array $params
     *
     * @return string
     *
     */
    public static function getDomainTldForRegister($params)
    {
        if ($params['original']['tld'] != '')
        {
            $tld = $params['original']['tld'];
        }
        else
        {
            $tld = $params['tld'];
        }

        return $tld;
    }
}