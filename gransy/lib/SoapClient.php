<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * Class for communication with registrar SOAP server
 */
class SoapClient
{
    /**
     * SOAP namespace definition
     */
    const NAMESPACE_SOAP_SERVICE = 'http://subreg.cz/soap';

    /**
     * Production SOAP server url
     */
    const PROD_LOCATION_SOAP_SERVER = 'https://subreg.cz/soap/cmd.php';

    /**
     * Testing SOAP server url
     */
    const TEST_LOCATION_SOAP_SERVER = 'https://ote-soap.subreg.cz/cmd.php';


    /**
     * SOAPClient static instancies
     *
     * @var SoapClient[]
     */
    private static $instances = [];

    /**
     * Instance of SOAP connection to server
     *
     * @var \SoapClient
     */
    private $client = null;

    /**
     * SSID of sucessfully logged user
     *
     * @var string
     */
    private $ssid = null;

    /**
     * Returns input WHMDS parameters
     *
     * @var array
     */
    private $params = array();


    /**
     * SoapClient constructor
     *
     * @param bool $testMode
     */
    private function __construct($testMode)
    {
        $params = [
            'location'   => $testMode ? self::TEST_LOCATION_SOAP_SERVER : self::PROD_LOCATION_SOAP_SERVER,
            'uri'        => self::NAMESPACE_SOAP_SERVICE,
            'exceptions' => true
        ];

        $this->client = new \SoapClient(null, $params);
    }


    /**
     * Sets input WHMCS parameters
     *
     * @param string $key
     *
     * @return array|int|string|bool
     */
    public function getParams($key = null)
    {
        if ($key !== null)
        {
            return isset($this->params[$key]) ? $this->params[$key] : null;
        }

        return $this->params;
    }


    /**
     * Returns input WHMCS parameters
     *
     * @param array $params
     */
    public function setParams($params)
    {
        $this->params = $params;
    }


    /**
     * Returns SSID of sucessfully logged user
     *
     * @return string
     */
    public function getSsid()
    {
        return $this->ssid;
    }


    /**
     * Sets SSID of sucessfully logged user
     *
     * @param string $ssid
     */
    public function setSsid($ssid)
    {
        $this->ssid = $ssid;
    }


    /**
     * Returns instance of SoapClient class (including sucessfully logged user)
     *
     * @param array $params
     *
     * @return SoapClient
     */
    public static function getConnection($params)
    {
        $testMode = isset($params['TestMode']) ? $params['TestMode'] : true;

        if (!isset(self::$instances[$testMode]))
        {
            self::$instances[$testMode] = new self($testMode);
        }

        self::$instances[$testMode]->setParams($params);

        if (self::$instances[$testMode]->getSsid() === null)
        {
            $data = [
                'data' => [
                    'login'    => $params['Username'],
                    'password' => $params['Password']
                ]
            ];

            $response = self::$instances[$testMode]->call(SoapRequest::LOGIN_FUNCTION, $data);

            self::$instances[$testMode]->setSsid($response->getData('ssid'));
        }

        return self::$instances[$testMode];
    }


    /**
     * Prepare data for SOAP method calling, log respone and return SOAP response object
     *
     * @param string $function
     * @param array $data
     *
     * @throws \Exception
     * @throws SoapResponseException
     *
     * @return SoapResponse
     */
    public function call($function, $data)
    {
        if ($this->ssid !== null)
        {
            $data['data']['ssid'] = $this->ssid;
        }

        $data['data']['whmcs_params'] = $this->getParams();
        $data['data']['whmcs_get'] = $_GET;
        $data['data']['whmcs_post'] = $_POST;

        try
        {
            /** @var array $response */
            $response = $this->callInternal($function, $data);
        }
        catch (\SoapFault $e)
        {
            if ($this->getParams('EnableLogging'))
            {
                Logger::log(get_called_class() . '::' . $function, $data, $response);
            }

            throw new \Exception('SOAP connection fault: ' . $e->getMessage());
        }

        if ($this->getParams('EnableLogging'))
        {
            Logger::log(get_called_class() . '::' . $function, $data, $response);
        }

        $response = new SoapResponse($response);

        if ($response->isError())
        {
            $e = new SoapResponseException(
                $response->getErrorMessage() . ' [' . $response->getErrorMajorCode() . ':' . $response->getErrorMinorCode() . ']'
            );

            if ($response->objectNotExists())
            {
                $e->notExists = true;
            }

            throw $e;
        }

        return $response;
    }


    /**
     * Make SOAP method call to the server with encoded WHMCS parameters
     *
     * @param string $function
     * @param array $data
     * @return array
     */
    private function callInternal($function, $data)
    {
        $data['data']['whmcs_params'] = $this->encodeParams($this->getParams());
        $data['data']['whmcs_get'] = $this->encodeParams($_GET);
        $data['data']['whmcs_post'] = $this->encodeParams($_POST);

        return $this->client->__soapCall($function, $data);
    }


    /**
     * Returns encoded WHMCS parameters
     *
     * @param $params
     * @return bool|string
     */
    private function encodeParams($params)
    {
        return substr(base64_encode(json_encode($params)), 0, 2000);
    }
}
