<?php

namespace WHMCS\Module\Registrar\Gransy;

/**
 * Helper class for formatting
 */
class Formatter
{
    /**
     * Available phone dial codes
     * @var array
     */
    static private $phoneCodes = [
        '93', '355', '213', '376', '244', '268', '54', '374', '61', '43', '994', '242', '973', '880', '246', '375', '32', '501', '229', '975', '591', '387', '267', '55', '673', '359', '226', '257', '855', '237', '1', '238', '236', '235', '56', '86', '57', '269', '243', '242', '506', '225', '385', '53', '357', '420', '45', '253', '767', '829', '593', '20', '503', '240', '291', '372', '251', '679', '358', '33', '241', '220', '995', '49', '233', '30', '473', '502', '224', '245', '592', '509', '504', '36', '354', '91', '62', '98', '964', '353', '972', '39', '876', '81', '962', '7', '254', '686', '850', '82', '965', '996', '856', '371', '961', '266', '231', '218', '423', '370', '352', '389', '261', '265', '60', '960', '223', '356', '692', '222', '230', '52', '691', '373', '377', '976', '382', '212', '258', '95', '264', '674', '977', '31', '64', '505', '227', '234', '47', '968', '92', '680', '507', '675', '595', '51', '63', '48', '351', '974', '40', '7', '250', '869', '758', '784', '685', '378', '239', '966', '221', '381', '248', '232', '65', '421', '386', '677', '252', '27', '34', '94', '249', '597', '268', '46', '41', '963', '992', '255', '66', '670', '228', '676', '868', '216', '90', '993', '688', '256', '380', '971', '44', '1', '598', '998', '678', '379', '58', '84', '967', '260', '263', '995', '886', '97', '392', '533', '252', '995', '61', '61', '672', '687', '689', '262', '590', '590', '508', '681', '682', '683', '690', '44', '44', '44', '264', '441', '246', '357', '284', '345', '500', '350', '664', '290', '649', '670', '939', '684', '671', '340', '852', '853', '298', '299', '594', '590', '596', '262', '18', '297', '599', '47', '247', '290', '381', '970', '212'
    ];


    /**
     * Formats phone number to global shape
     *
     * @param string $phone
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function formatPhone($phone)
    {
        if (strpos($phone, '00') === 0)
        {
            foreach (self::$phoneCodes as $number)
            {
                if (strpos($phone, '00' . $number) === 0)
                {
                    return '+' . $number . '.' . substr($phone, strlen($number) + 2);
                }
            }

            throw new \Exception('Unknown international phone code: ' . $phone);
        }
        elseif (strpos($phone, '+') === 0)
        {
            foreach (self::$phoneCodes as $number)
            {
                if (strpos($phone, '+' . $number . '.') === 0)
                {
                    return '+' . $number . '.' . substr($phone, strlen($number) + 2);
                }

                if (strpos($phone, '+' . $number) === 0)
                {
                    return '+' . $number . '.' . substr($phone, strlen($number) + 1);
                }
            }

            throw new \Exception('Unknown international phone code: ' . $phone);
        }
        else
        {
            return '+1.' . $phone;
        }
    }


    /**
     * Returns formatted authId
     *
     * @param string $authId
     *
     * @return string
     */
    public static function formatAuthId($authId)
    {
        return urlencode(str_replace(array('&#039;', '&#034;'), array('\'', '"'), $authId));
    }


    /**
     * Returns formatted street
     *
     * @param string $address1
     * @param string $address2
     *
     * @return string
     */
    public static function formatStreet($address1, $address2)
    {
        return trim(implode(' ', array($address1, $address2)));
    }


    /**
     * Returns domain name in punycode
     *
     * @param string $domainName
     * @return string
     */
    public static function encodeDomainName($domainName)
    {
        $idn = new IDNAConvert();

        return $idn->encode($domainName);

    }
}