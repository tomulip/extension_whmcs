<?php

namespace WHMCS\Gransy;

/**
 * WHMCS SDK Gransy Registrar Module
 *
 * For more information about WHCMS, please refer to the online documentation.
 *
 * @see https://developers.whmcs.com/domain-registrars/
 *
 * @copyright Gransy -  (C) 2011-2017
 *
 * @version 2017-08-24 09:53:41
 *
 * @license https://www.whmcs.com/license/ WHMCS Eula
 */

use WHMCS\Module\Registrar\Gransy\AdditionalData;
use WHMCS\Module\Registrar\Gransy\Formatter;
use WHMCS\Module\Registrar\Gransy\ServiceProvider;
use WHMCS\Module\Registrar\Gransy\SoapResponseException;
use WHMCS\Module\Registrar\Gransy\SoapRequest;


require_once 'lib/AdditionalData.php';
require_once 'lib/DatabaseManager.php';
require_once 'lib/Formatter.php';
require_once 'lib/IDNAConvert.php';
require_once 'lib/Logger.php';
require_once 'lib/ServiceProvider.php';
require_once 'lib/SoapClient.php';
require_once 'lib/SoapRequest.php';
require_once 'lib/SoapResponse.php';
require_once 'lib/SoapResponseException.php';

ServiceProvider::init();


/**
 * Define module related metadata
 *
 * Provide some module information including the display name and API Version to
 * determine the method of decoding the input values.
 *
 * @return array
 */
function gransy_MetaData()
{
    return array(
        'DisplayName' => 'Gransy Registrar Module for WHMCS',
        'APIVersion' => '1.1',
    );
}


/**
 * Define registrar configuration options.
 *
 * The values you return here define what configuration options
 * we store for the module. These values are made available to
 * each module function.
 *
 * You can store an unlimited number of configuration settings.
 * The following field types are supported:
 *  * Text
 *  * Password
 *  * Yes/No Checkboxes
 *  * Dropdown Menus
 *  * Radio Buttons
 *  * Text Areas
 *
 * @see https://developers.whmcs.com/domain-registrars/config-options/
 *
 * @return array
 */
function gransy_getConfigArray()
{
    return [
        'FriendlyName' => [
            'Type' => 'System', 
            'Value' => 'Gransy'
        ],
        'Username' => [
            'Type' => 'text', 
            'Size' => '20', 
            'Description' => 'Enter your username here'
        ],
        'Password' => [
            'Type' => 'password', 
            'Size' => '20', 
            'Description' => 'Enter your password here'
        ],
        'TestMode' => [
            'FriendlyName' => 'Test mode',
            'Type' => 'yesno'
        ],
        'EnableLogging' => [
            'FriendlyName' => 'Enable logging',
            'Type' => 'yesno'
        ]
    ];
}


/**
 * Register a domain.
 *
 * Attempt to register a domain with the domain registrar.
 *
 * This is triggered when the following events occur:
 * * Payment received for a domain registration order
 * * When a pending domain registration order is accepted
 * * Upon manual request by an admin user
 *
 * @param array $params common module parameters
 * @param bool $transfer transfer domain sign
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_RegisterDomain($params, $transfer = false)
{
    $values = ['success' => true];

    try
    {
        $domain = ServiceProvider::getDomainNameForRegister($params);

        $admin = [
            'name'      => $params['adminfirstname'],
            'surname'   => $params['adminlastname'],
            'org'       => $params['admincompanyname'],
            'street'    => Formatter::formatStreet($params['adminaddress1'], $params['adminaddress2']),
            'city'      => $params['admincity'],
            'pc'        => $params['adminpostcode'],
            'cc'        => $params['admincountry'],
            'email'     => $params['adminemail'],
            'phone'     => $params['adminfullphonenumber'] ? $params['adminfullphonenumber'] : Formatter::formatPhone($params['adminphonenumber'])
        ];

        if ($params['adminstate'])
        {
            $admin['sp'] = $params['adminstate'];
        }

        $reg = [
            'name'      => $params['firstname'],
            'surname'   => $params['lastname'],
            'org'       => $params['companyname'],
            'street'    => Formatter::formatStreet($params['address1'], $params['address2']),
            'city'      => $params['city'],
            'pc'        => $params['postcode'],
            'cc'        => $params['country'],
            'email'     => $params['email'],
            'phone'     => $params['fullphonenumber'] ? $params['fullphonenumber'] : Formatter::formatPhone($params['phonenumber'])
        ];

        if ($params['state'])
        {
            $reg['sp'] = $params['state'];
        }

        $nss = [];

        if ($params['ns1'])
        {
            $nss[]['hostname'] = $params['ns1'];
        }

        if ($params['ns2'])
        {
            $nss[]['hostname'] = $params['ns2'];
        }

        if ($params['ns3'])
        {
            $nss[]['hostname'] = $params['ns3'];
        }

        if ($params['ns4'])
        {
            $nss[]['hostname'] = $params['ns4'];
        }

        $nsset = '';

        if (isset($params['ns1']) && !isset($params['ns2']) && !isset($params['ns3']) && !isset($params['ns4']))
        {
            $nsset = $params['ns1'];
        }

        $data = [
            'data' => [
                'order' => [
                    'domain' => Formatter::encodeDomainName($domain),
                    'params' => [
                        'params' => [
                            'whmcs'         => 1,
                            'whoisproxy'    => isset($params['idprotection']) ? 1 : 0
                        ]
                    ]
                ]
            ]
        ];

        $additionalData = new AdditionalData(
            ServiceProvider::getDomainTldForRegister($params), $params['additionalfields']
        );

        if ($additionalData->hasData())
        {
            foreach ($additionalData->getData() as $key => $value)
            {
                $data['data']['order']['params']['params'][$key] = $value;
            }
        }

        if ($transfer)
        {
            $data['data']['order']['type'] = SoapRequest::TRANSFER_DOMAIN_MAKE_ORDER_TYPE;
            $data['data']['order']['params']['operation'] = 'request';
            $data['data']['order']['params']['authid'] = Formatter::formatAuthId($params['transfersecret']);

            $data['data']['order']['params']['new']['registrant']['new'] = $reg;
            $data['data']['order']['params']['new']['contacts']['billing']['new'] = $admin;
            $data['data']['order']['params']['new']['contacts']['admin']['new'] = $admin;
            $data['data']['order']['params']['new']['contacts']['tech']['new'] = $admin;
        }
        else
        {
            $data['data']['order']['type'] = SoapRequest::CREATE_DOMAIN_MAKE_ORDER_TYPE;

            $data['data']['order']['params']['ns']['hosts'] = $nss;

            if ($nsset)
            {
                $data['data']['order']['params']['ns']['nsset'] = $nsset;
            }

            $data['data']['order']['params']['period'] = $params['regperiod'];
            $data['data']['order']['params']['registrant']['new'] = $reg;
            $data['data']['order']['params']['contacts']['billing']['new'] = $admin;
            $data['data']['order']['params']['contacts']['admin']['new'] = $admin;
            $data['data']['order']['params']['contacts']['tech']['new'] = $admin;

            if ($params['premiumEnabled'] && $params['premiumCost'])
            {
                $data['data']['order']['params']['params']['accepted_premium_cost'] = $params['premiumCost'];
            }
        }

        $request = new SoapRequest(SoapRequest::MAKE_ORDER_FUNCTION, $params, $data);

        $response = ServiceProvider::sendRequest($request);
        
        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] .= $e->getMessage();
    }

    return $values;
}


/**
 * Get Domain Suggestions.
 *
 * Provide domain suggestions based on the domain lookup term provided.
 *
 * @param array $params common module parameters
 * @see https://developers.whmcs.com/domain-registrars/module-parameters/
 *
 * @see \WHMCS\Domains\DomainLookup\SearchResult
 * @see \WHMCS\Domains\DomainLookup\ResultsList
 *
 * @throws Exception Upon domain suggestions check failure.
 *
 * @return \WHMCS\Domains\DomainLookup\ResultsList
 */
function gransy_GetDomainSuggestions($params)
{
    return ServiceProvider::checkDomainAvailability($params);
}


/**
 * Check Domain Availability.
 *
 * Determine if a domain or group of domains are available for
 * registration or transfer.
 *
 * @param array $params common module parameters
 * @see https://developers.whmcs.com/domain-registrars/availability-checks/
 *
 * @throws Exception Upon domain availability check failure.
 *
 * @return \WHMCS\Domains\DomainLookup\ResultsList
 */
function gransy_CheckAvailability($params)
{
    return ServiceProvider::checkDomainAvailability($params);
}


/**
 * Domain Suggestion Settings.
 *
 * Defines the settings relating to domain suggestions (optional).
 * It follows the same convention as `getConfigArray`.
 *
 * @see https://developers.whmcs.com/domain-registrars/check-availability/
 *
 * @return array of Configuration Options
 */
function gransy_DomainSuggestionOptions()
{
    return [
        'suggestMaxResultCount' => [
            'FriendlyName' => 'Maximum Number of Suggestions to Return',
            'Type' => 'dropdown',
            'Options' => '10,25,50,75,100',
            'Default' => '10'
        ],
    ];
}


/**
 * Initiate domain transfer.
 *
 * Attempt to create a domain transfer request for a given domain.
 *
 * This is triggered when the following events occur:
 * * Payment received for a domain transfer order
 * * When a pending domain transfer order is accepted
 * * Upon manual request by an admin user
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_TransferDomain($params)
{
    return gransy_RegisterDomain($params, true);
}


/**
 * Renew a domain.
 *
 * Attempt to renew/extend a domain for a given number of years.
 *
 * This is triggered when the following events occur:
 * * Payment received for a domain renewal order
 * * When a pending domain renewal order is accepted
 * * Upon manual request by an admin user
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_RenewDomain($params)
{
    $values = ['success' => true];

    try
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        $data = [
            'data' => [
                'order' => [
                    'type' => SoapRequest::RENEW_DOMAIN_MAKE_ORDER_TYPE,
                    'domain' => Formatter::encodeDomainName($domain),
                    'params' => [
                        'period' => $params['regperiod'],
                        'params' => [
                            'whmcs' => 1
                        ]
                    ]
                ]
            ]
        ];

        if ($params['premiumEnabled'] && $params['premiumCost'])
        {
            $data['data']['order']['params']['params']['accepted_premium_cost'] = $params['premiumCost'];
        }

        $request = new SoapRequest(SoapRequest::MAKE_ORDER_FUNCTION, $params, $data);

        $response = ServiceProvider::sendRequest($request);

        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] .= $e->getMessage();
    }

    return $values;
}


/**
 * Fetch current nameservers.
 *
 * This function should return an array of nameservers for a given domain.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_GetNameservers($params)
{
    $values = [
        'ns1'       => '',
        'ns2'       => '',
        'ns3'       => '',
        'ns4'       => ''
    ];

    try
    {
        $domainInfo = ServiceProvider::getDomainInfo($params);

        $i = 1;

        if ($domainInfo->getData('hosts'))
        {
            foreach ($domainInfo->getData('hosts') as $ns)
            {
                $values['ns' . $i++] = $ns;
            }
        }
        else
        {
            $options = $domainInfo->getData('options');

            if (isset($options['nsset']))
            {
                $request = new SoapRequest(
                    SoapRequest::INFO_OBJECT_FUNCTION,
                    $params,
                    [
                        'data' => [
                            'object' => SoapRequest::INFO_OBJECT_NSSET,
                            'id'     => $options['nsset']
                        ]
                    ]
                );

                $response = ServiceProvider::sendRequest($request);

                $nsset = $response->getData('nsset');

                foreach ($nsset['ns'] as $ns)
                {
                    $values['ns' . $i++] = $ns['host'];
                }
            }
        }
    }
    catch (SoapResponseException $e)
    {
        $values['error'] .= $e->getMessage();

        if ($e->notExists)
        {
            $values['error'] .= '&nbsp;&nbsp;<strong>Probably not yet registered</strong>';
        }
    }
    catch (Exception $e)
    {
        $values['error'] .= $e->getMessage();
    }

    $error = ServiceProvider::getOrderErrorHtmlCode($params, $values['error']);

    if (!empty($error))
    {
        $values['error'] .= $error;
    }

    return $values;
}


/**
 * Save nameserver changes.
 *
 * This function should submit a change of nameservers request to the
 * domain registrar.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_SaveNameservers($params)
{
    $values = ['success' => true];

    try
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        $data = [
            'data' => [
                'order' => [
                    'domain' => Formatter::encodeDomainName($domain),
                    'type'   => SoapRequest::MODIFY_NS_DOMAIN_MAKE_ORDER_TYPE,
                    'params' => [
                        'params' => [
                            'whmcs' => 1
                        ]
                    ]
                ]
            ]
        ];

        if (isset($params['ns1']) && !isset($params['ns2']) && !isset($params['ns3']) && !isset($params['ns4']))
        {
            $data['data']['order']['params']['ns']['nsset'] = $params['ns1'];
        }
        else
        {
            $nss = [];

            if ($params['ns1'])
            {
                $nss[]['hostname'] = $params['ns1'];
            }

            if ($params['ns2'])
            {
                $nss[]['hostname'] = $params['ns2'];
            }

            if ($params['ns3'])
            {
                $nss[]['hostname'] = $params['ns3'];
            }

            if ($params['ns4'])
            {
                $nss[]['hostname'] = $params['ns4'];
            }

            $data['data']['order']['params']['ns']['hosts'] = $nss;
        }

        $request = new SoapRequest(SoapRequest::MAKE_ORDER_FUNCTION, $params, $data);

        $response = ServiceProvider::sendRequest($request);

        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Get the current WHOIS Contact Information.
 *
 * Should return a multi-level array of the contacts and name/address
 * fields that be modified.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_GetContactDetails($params)
{
    $values = [];

    try
    {
        $domainInfo = ServiceProvider::getDomainInfo($params);

        $registrant = $domainInfo->getData('registrant');

        $contactIds = [
            'registrant' => $registrant['subregid']
        ];

        if (is_array($domainInfo->getData('contacts')))
        {
            foreach ($domainInfo->getData('contacts') as $type => $contacts)
            {
                $contact = current($contacts);

                $contactIds[$type] = $contact['subregid'];
            }
        }

        $contactDetails = [];

        foreach ($contactIds as $type => $id)
        {
            if (!isset($contactDetails[$id]))
            {
                $request = new SoapRequest(
                    SoapRequest::INFO_CONTACT_FUNCTION,
                    $params,
                    [
                        'data' => [
                            'contact' => [
                                'id' => $id
                            ]
                        ]
                    ]
                );

                $contactDetails[$id] = ServiceProvider::sendRequest($request);
            }

            $formattedType = ($type === 'tech') ? 'Technical' : ucfirst($type);

            $values[$type] = [
                'First Name'        => $contactDetails[$id]->getData('name'),
                'Last Name'         => $contactDetails[$id]->getData('surname'),
                'Organisation Name' => $contactDetails[$id]->getData('org'),
                'Address'           => $contactDetails[$id]->getData('street'),
                'City'              => $contactDetails[$id]->getData('city'),
                'ZIP Code'          => $contactDetails[$id]->getData('pc'),
                'Region'            => $contactDetails[$id]->getData('sp'),
                'Country'           => $contactDetails[$id]->getData('cc'),
                'Email'             => $contactDetails[$id]->getData('email'),
                'Phone'             => $contactDetails[$id]->getData('phone'),
                'Fax'               => $contactDetails[$id]->getData('fax')
            ];
        }
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;

}


/**
 * Update the WHOIS Contact Information for a given domain.
 *
 * Called when a change of WHOIS Information is requested within WHMCS.
 * Receives an array matching the format provided via the `GetContactDetails`
 * method with the values from the users input.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_SaveContactDetails($params)
{
    $values = ['success' => true];

    try
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        $data = [
            'data' => [
                'order' => [
                    'domain' => Formatter::encodeDomainName($domain),
                    'type'   => SoapRequest::MODIFY_DOMAIN_MAKE_ORDER_TYPE ,
                    'params' => [
                        'params' => [
                            'whmcs' => 1
                        ]
                    ]
                ]
            ]
        ];

        foreach ($params['contactdetails'] as $type => $detail)
        {
            $contact = [
                'name'      => $detail['First Name'],
                'surname'   => $detail['Last Name'],
                'org'       => $detail['Organisation Name'],
                'street'    => $detail['Address'],
                'city'      => $detail['City'],
                'pc'        => $detail['ZIP Code'],
                'sp'        => $detail['Region'],
                'cc'        => $detail['Country'],
                'email'     => $detail['Email'],
                'phone'     => Formatter::formatPhone($detail['Phone']),
                'fax'       => $detail['Fax']
            ];

            $type = ($type === 'Technical') ? 'tech' : strtolower($type);

            if ($type == 'registrant')
            {
                $data['data']['order']['params']['registrant']['new'] = $contact;
            }
            else
            {
                $data['data']['order']['params']['contacts'][$type]['new'] = $contact;
            }
        }

        $request = new SoapRequest(SoapRequest::MAKE_ORDER_FUNCTION, $params, $data);

        $response = ServiceProvider::sendRequest($request);

        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Request EEP Code.
 *
 * Supports displaying the EPP Code directly to a user.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 *
 */
function gransy_GetEPPCode($params)
{
    $values = [];

    try
    {
        $domainInfo = ServiceProvider::getDomainInfo($params);

        if ($domainInfo->getData('authid'))
        {
            $values['eppcode'] = $domainInfo->getData('authid');
        }
        else
        {
            throw new Exception('Cannot get EPP code (probably not supported for this registry)');
        }
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Get registrar lock status.
 *
 * Also known as Domain Lock or Transfer Lock status.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return string|array Lock status or error message
 */
function gransy_GetRegistrarLock($params)
{
    try
    {
        $domainInfo = ServiceProvider::getDomainInfo($params);

        if (in_array('clientTransferProhibited', $domainInfo->getData('status')))
        {
            return 'locked';
        }
        else
        {
            $request = new SoapRequest(
                SoapRequest::PRICES_FUNCTION,
                $params,
                [
                    'data' => [
                        'tld' => $params['tld']
                    ]
                ]
            );

            $response = ServiceProvider::sendRequest($request);

            if (in_array('clientTransferProhibited', $response->getData('statuses')))
            {
                return 'unlocked';
            }
            else
            {
                return '';
            }
        }
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Set registrar lock status.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_SaveRegistrarLock($params)
{
    $values = ['success' => true];

    try
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        $domainInfo = ServiceProvider::getDomainInfo($params);

        if (in_array('clientTransferProhibited', $domainInfo->getData('status')))
        {
            $type = SoapRequest::UNLOCK_DOMAIN_MAKE_ORDER_TYPE;
        }
        else
        {
            $type = SoapRequest::LOCK_DOMAIN_MAKE_ORDER_TYPE;
        }

        $request = new SoapRequest(
            SoapRequest::MAKE_ORDER_FUNCTION,
            $params,
            [
                'data' => [
                    'order' => [
                        'domain' => Formatter::encodeDomainName($domain),
                        'type'   => $type,
                        'params' => [
                            'params' => [
                                'whmcs' => 1
                            ]
                        ]
                    ]
                ]
            ]
        );

        $response = ServiceProvider::sendRequest($request);

        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Get DNS Records for DNS Host Record Management.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array DNS Host Records
 */
function gransy_GetDNS($params)
{
    $values = [];

    try
    {
        $request = new SoapRequest(
            SoapRequest::GET_DNS_ZONE_FUNCTION,
            $params,
            [
                'data' => [
                    'domain' => $params['sld'] . '.' . $params['tld']
                ]
            ]
        );

        $response = ServiceProvider::sendRequest($request);

        foreach ($response->getData('records') as $record)
        {
            if ($record['type'] == 'REDIR')
            {
                $record['type'] = 'URL';
            }

            if ($record['type'] == 'PROXY')
            {
                $record['type'] = 'FRAME';
            }

            $values[] = array(
                'hostname'  => $record['name'] ? $record['name'] : '@',
                'type'      => $record['type'],
                'address'   => $record['content'],
                'ttl'       => $record['ttl'],
                'prio'      => $record['prio'],
                'priority'  => $record['prio']
            );
        }
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Update DNS Host Records.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_SaveDNS($params)
{
    $values = ['success' => true];

    try
    {
        $records = [];

        foreach ($params['dnsrecords'] as $key => $values)
        {
            $prio = '';

            if (isset($values['prio']))
            {
                $prio = $values['prio'];
            }
            elseif (isset($values['priority']))
            {
                $prio = $values['priority'];
            }

            if ($prio == 'N/A')
            {
                $prio = 0;
            }

            if ($values['type'] == 'URL')
            {
                $values['type'] = 'REDIR';
            }

            if ($values['type'] == 'FRAME')
            {
                $values['type'] = 'PROXY';
            }

            $record = [
                'prio' => $prio
            ];

            if (isset($values['hostname']))
            {
                $record['name'] = $values['hostname'];
            }

            if (isset($values['type']))
            {
                $record['type'] = $values['type'];
            }

            if (isset($values['address']))
            {
                $record['content'] = $values['address'];
            }

            if (isset($values['ttl']))
            {
                $record['ttl'] = $values['ttl'];
            }

            $records[] = $record;
        }

        $request = new SoapRequest(
            SoapRequest::SET_DNS_ZONE_FUNCTION,
            $params,
            [
                'data' => [
                    'domain'  => $params['sld'] . '.' . $params['tld'],
                    'records' => $records
                ]
            ]
        );

        ServiceProvider::sendRequest($request);
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Returns email forwarding for specific domain.
 *
 * @param array $params common module parameters
 *
 * @return array
 */
function gransy_GetEmailForwarding($params)
{
    $values = [];

    try
    {
        $request = new SoapRequest(
            SoapRequest::GET_EMAIL_FORWARDING_FUNCTION,
            $params,
            [
                'data' => [
                    'domain' => $params['sld'] . '.' . $params['tld']
                ]
            ]
        );

        $response = ServiceProvider::sendRequest($request);

        foreach ($response->getData('rules') as $rule)
        {
            $values[] = array(
                'prefix'    => $rule['prefix'],
                'forwardto' => $rule['destination']
            );
        }
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Sets email forwarding for specific domain.
 *
 * @param array $params common module parameters
 *
 * @return array
 */
function gransy_SaveEmailForwarding($params)
{
    $values = ['success' => true];

    try
    {
        $all = [];

        foreach ($params['prefix'] as $key => $prefix)
        {
            if (is_array($prefix))
            {
                foreach ($prefix as $key2 => $prefix2)
                {
                    $all[$prefix2] = $params['forwardto'][$key][$key2];
                }
            }
            else
            {
                $all[$prefix] = $params['forwardto'][$key];
            }
        }

        $rules = [];

        foreach ($all as $prefix => $destination)
        {
            if ($prefix != '' && $destination != '')
            {
                $rules[] = array(
                    'prefix'      => $prefix,
                    'destination' => $destination
                );
            }
        }

        $request = new SoapRequest(
            SoapRequest::SET_EMAIL_FORWARDING_FUNCTION,
            $params,
            [
                'data' => [
                    'domain' => $params['sld'] . '.' . $params['tld'],
                    'rules'  => $rules
                ]
            ]
        );

        ServiceProvider::sendRequest($request);
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Enable/Disable ID Protection.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/function-index/
 *
 * @return array
 */
function gransy_IDProtectToggle($params)
{
    $values = [];

    try
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        if ($_GET['conf'] == 'removedidprotect')
        {
            $whoisproxy = 0;
        }
        elseif ($_GET['conf'] == 'addedidprotect')
        {
            $whoisproxy = 1;
        }
        else
        {
            $whoisproxy = $params['protectenable'] ? 1 : 0;
        }

        $request = new SoapRequest(
            SoapRequest::MAKE_ORDER_FUNCTION,
            $params,
            [
                'data' => [
                    'order' => [
                        'domain' => Formatter::encodeDomainName($domain),
                        'type'   => SoapRequest::MODIFY_DOMAIN_MAKE_ORDER_TYPE,
                        'params' => [
                            'params' => [
                                'whoisproxy' => $whoisproxy,
                                'whmcs'      => 1
                            ]
                        ]
                    ]
                ]
            ]
        );

        $response = ServiceProvider::sendRequest($request);

        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Send EEP Code.
 *
 * Supports indicating that the EPP Code will be emailed to the registrant.
 *
 * @param array $params common module parameters
 *
 * @return array
 */
function gransy_SendEPPCode($params)
{
    $values = ['success' => true];

    try
    {
        $domain = $params['sld'] . '.' . $params['tld'];

        $request = new SoapRequest(
            SoapRequest::MAKE_ORDER_FUNCTION,
            $params,
            [
                'data' => [
                    'order' => [
                        'type'   => SoapRequest::SEND_AUTH_ID_MAKE_ORDER_TYPE,
                        'domain' => Formatter::encodeDomainName($domain),
                        'params' => [
                            'params' => [
                                'whmcs' => 1
                            ]
                        ]
                    ]
                ]
            ]
        );

        $response = ServiceProvider::sendRequest($request);

        ServiceProvider::createOrder($domain, $response->getData('orderid'));
    }
    catch (Exception $e)
    {
        $values['error'] = $e->getMessage();
    }

    return $values;
}


/**
 * Sync Domain Status & Expiration Date.
 *
 * Domain syncing is intended to ensure domain status and expiry date
 * changes made directly at the domain registrar are synced to WHMCS.
 * It is called periodically for a domain.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/domain-syncing/
 *
 * @return array
 */
function gransy_Sync($params)
{
    $request = new SoapRequest(SoapRequest::WHMCS_SYNC_FUNCTION, $params, ['data' => $params]);

    $response = ServiceProvider::sendRequest($request);

    return $response->getData();
}


/**
 * Incoming Domain Transfer Sync.
 *
 * Check status of incoming domain transfers and notify end-user upon
 * completion. This function is called daily for incoming domains.
 *
 * @param array $params common module parameters
 *
 * @see https://developers.whmcs.com/domain-registrars/domain-syncing/
 *
 * @return array
 */
function gransy_TransferSync($params)
{
    $request = new SoapRequest(SoapRequest::WHMCS_TRANSFER_SYNC_FUNCTION, $params, ['data' => $params]);

    $response = ServiceProvider::sendRequest($request);

    return $response->getData();
}


/**
 * Client Area Custom Button Array.
 *
 * We register a SendEEPCode action which triggers
 * the `gransy_SendEPPCode` function when invoked.
 *
 * @see https://developers.whmcs.com/domain-registrars/extending-further/
 *
 * @return array
 */
function gransy_ClientAreaCustomButtonArray()
{
    return array(
        'Send EPP Code' => 'SendEPPCode'
    );
}


/**
 * Admin Area Custom Button Array.
 *
 * We register a SendEEPCode action which triggers
 * the `gransy_SendEPPCode` function when invoked.
 *
 * @see https://developers.whmcs.com/domain-registrars/extending-further/
 *
 * @return array
 */
function gransy_AdminCustomButtonArray()
{
    return array(
        'Send EPP Code' => 'SendEPPCode'
    );
}
