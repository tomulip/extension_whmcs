<?php

/**
 * Additional domains fields
 *
 * Gransy -  (C) 2011-2017
 */


$additionaldomainfields['.abogado'][] = array (
  'Name' => 'law-accreditation-b',
  'DisplayName' => 'Accreditation Body',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.abogado'][] = array (
  'Name' => 'law-accreditation-id',
  'DisplayName' => 'Accreditation ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.abogado'][] = array (
  'Name' => 'law-accredita-year',
  'DisplayName' => 'Accreditation Year',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.abogado'][] = array (
  'Name' => 'law-jurisdiction-country',
  'DisplayName' => 'Country Jurisdiction',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.academy'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.accountants'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.actor'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.aero'][] = array (
  'Name' => 'aero-id',
  'DisplayName' => 'Account ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.aero'][] = array (
  'Name' => 'aero-key',
  'DisplayName' => 'Account Password',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.agency'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.airforce'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.al'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.apartments'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.ar'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.army'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org.ar'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.art'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',ja|japan / japonština,he|hebrew / hebrejština,ar|arabic / arabština,ru|russian / ruština,th|thai / thajština,lo|lao / laoština,zh|chinese / čínština,ko|korean / korejština,latn|latin / latinka,grek|greek / řečtina',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.associates'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.attorney'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.auction'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.auto'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',Cyrl|Cyrillic,de|German,es|Spanish,fr|French,it|Italian,pt|Portuguese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.az'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'ID number of the owner',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ba'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ba'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.band'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bargains'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bayern'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bb'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.be'][] = array (
  'Name' => 'quarantine',
  'DisplayName' => 'Transfer z karantény',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.berlin'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bg'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'ID number or passport number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.bg'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT/social number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.bh'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bike'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bingo'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.biz'][] = array (
  'Name' => 'lang_biz',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',da|Danish,de|German,es|Spanish,fi|Finnish,hu|Hungarian,is|Icelandic,ja|Japanese,ko|Korean,lt|Lithuanian,lv|Latvian,no|Norwegian,pl|Polish,pt|Portuguese,sv|Swedish,zh|Chinese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.boutique'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.br'][] = array (
  'Name' => 'CNPJ',
  'DisplayName' => 'CNPJ (Brazilian ID number)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.br'][] = array (
  'Name' => 'CPF',
  'DisplayName' => 'CPF (Brazilian VAT)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.br'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.net.br'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.builders'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.business'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.by'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'ID number of the owner',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.by'][] = array (
  'Name' => 'issue_date',
  'DisplayName' => 'Date of passport issue or registration in EGR',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.by'][] = array (
  'Name' => 'issue_org',
  'DisplayName' => 'Issuer of the passport or the organization providing the registration in EGR',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.by'][] = array (
  'Name' => 'passport',
  'DisplayName' => 'Passport number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.by'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ca'][] = array (
  'Name' => 'legaltype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',CCO|Corporation (Canada or Canadian province or territory),ABO|The indigenous peoples of Canada,ASS|Canadian Unincorporated Association,CCT|Canadian citizen,EDU|Canadian Educational Institution,GOV|Government or government entity in Canada,HOP|Canadian Hospital,INB|Indian minority accepting Canadian Indian Act,LAM|Canadian Library; Archive or Museum,LGR|Canadian citizen or a citizen with permanent residence,MAJ|Her Majesty the Queen,OMK|Official trade-mark registered in Canada,PLT|Canadian Political Party,PRT|Company registered in Canada,RES|Permanent resident of Canada,TDM|Trade-mark registered in Canada (by a non-Canadian owner),TRD|Canadian Trade Union,TRS|Foundation / Cartel established in Canada',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.cab'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cafe'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cam'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',ar|Arabic,az|Azerština,bg|Bulgarian,ca|Katalánština,cs|Čeština,da|Danish,de|German,el|Řečtina,es|Spanish,et|Estonian,fi|Finnish,fr|French,he|Hebrew,hr|Croatian,hu|Hungarian,hy|Armenian,is|Icelandic,it|Italian,ja|Japanese,ka|Georgian,ko|Korean,la|Latinština,lb|Lucemburština,lt|Lithuanian,lv|Latvian,mk|Makedonština,nl|Holandština,no|Norwegian,pl|Polish,pt|Portuguese,ro|Romanian,ro-MD|Moldavian,ru|Russian,sk|Slovak,sl|Slovenian,sq|Albanian,sr|Serbian,sv|Švédština,tavt|Tai Viet,th|Thai,tr|Turkish,uk|Ukrainian',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.camera'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.camp'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.capital'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.car'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',Cyrl|Cyrillic,de|German,es|Spanish,fr|French,it|Italian,pt|Portuguese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cards'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.care'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.careers'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cars'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',Cyrl|Cyrillic,de|German,es|Spanish,fr|French,it|Italian,pt|Portuguese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cash'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.casino'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cat'][] = array (
  'Name' => 'reason',
  'DisplayName' => 'Text explaining the intented use in English',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.catering'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.center'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.city'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.claims'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cleaning'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.clinic'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.clothing'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.club'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',es|Spanish,zh|Čínština,ja|Japanese,pl|Polish,ru|Russian,ko|Korean,lt|Lithuanian,lv|Latvian,hu|Hungarian',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cn'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.cn'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.net.cn'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org.cn'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.sh.cn'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.coach'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latn|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.codes'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.coffee'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',CZE|Czech,SLO|Slovak,AFR|Afrikaans,ALB|Albanian,ARA|Arabic,ARG|Aragonese,ARM|Armenian,ASM|Assamese,AST|Asturian,AVE|Avestan,AWA|Awadhi,AZE|Azerbaijani,BAN|Balinese,BAL|Baluchi,BAS|Basa,BAK|Bashkir,BAQ|Basque,BEL|Belarusian,BEN|Bengali,BHO|Bhojpuri,BOS|Bosnian,BUL|Bulgarian,BUR|Burmese,CAR|Carib,CAT|Catalan,CHE|Chechen,CHI|Chinese,CHV|Chuvash,COP|Coptic,COS|Corsican,SCR|Croatian,DAN|Danish,DIV|Divehi,DOI|Dogri,DUT|Dutch,ENG|English,EST|Estonian,FAO|Faroese,FIJ|Fijian,FIN|Finnish,FRE|French,FRY|Frisian,GLA|Gaelic,GEO|Georgian,GER|German,GON|Gondi,GUJ|Gujarati,HEB|Hebrew,HIN|Hindi,HUN|Hungarian,ICE|Icelandic,INC|Indic,IND|Indonesian,INH|Ingush,GLE|Irish,ITA|Italian,JPN|Japanese,JAV|Javanese,KAS|Kashmiri,KAZ|Kazakh,KHM|Khmer,KIR|Kirghiz,KOR|Korean,KUR|Kurdish,LAO|Lao,LAV|Latvian,LIT|Lithuanian,LTZ|Luxembourgish,MAC|Macedonian-MAC,MAL|Malayalam,MAY|Malay,MLT|Maltese,MAO|Maori,MOL|Moldavian,MON|Mongolian,NEP|Nepali,NOR|Norwegian,ORI|Oriya,OSS|Ossetian,PAN|Panjabi,PER|Persian,POL|Polish,POR|Portuguese,PUS|Pushto,RAJ|Rajasthani,RUM|Romanian,RUS|Russian,SMO|Samoan,SAN|Sanskrit,SRD|Sardinian,SCC|Serbian,SND|Sindhi,SIN|Sinhalese,SLV|Slovenian,SOM|Somali,SPA|Spanish,SWA|Swahili,SWE|Swedish,SYR|Syriac,TGK|Tajik,TAM|Tamil,TEL|Telugu,THA|Thai,TIB|Tibetan,TUR|Turkish,UKR|Ukrainian,URD|Urdu,UZB|Uzbek,VIE|Vietnamese language,WEL|Welsh,YID|Yiddish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.community'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.company'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.computer'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.condos'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.construction'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.consulting'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.contractors'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cool'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.coupons'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cr'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.credit'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.creditcard'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cruises'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.cy'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.dance'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.dating'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.de'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.deals'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.degree'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.delivery'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.democrat'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.dental'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.dentist'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.diamonds'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.digital'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.direct'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.directory'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.discount'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.dk'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.doctor'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.dog'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.domains'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.education'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ee'][] = array (
  'Name' => 'authid',
  'DisplayName' => 'AuthID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ee'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ee'][] = array (
  'Name' => 'idnum_type',
  'DisplayName' => 'Type of ID',
  'Type' => 'dropdown',
  'Options' => ',op|Identity card,passport|Passport,ico|ID,birthday|Date of birth',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ee'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.ee'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.email'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.energy'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.engineer'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.engineering'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.enterprises'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.equipment'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.es'][] = array (
  'Name' => 'ES-NIF-NIE',
  'DisplayName' => 'NIF / NIE number for owner contacts from ES',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.es'][] = array (
  'Name' => 'IDCARD-OR-PASSPORT-NUMBER',
  'DisplayName' => 'ID number or passport number for owner contacts outside ES',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.estate'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.eu'][] = array (
  'Name' => 'lang_eu',
  'DisplayName' => 'Language:',
  'Type' => 'dropdown',
  'Options' => 'cs|Čeština,sk|Slovak,da|Danish,nl|Dutch,en|English,et|Estonian,fi|Finnish,fr|French,de|German,el|Greek,hu|Hungarian,it|Italian,lv|Latvian,lt|Lithuanian,mt|Maltese,pl|Polish,pt|Portuguese,ro|Romanian,sl|Slovenian,es|Spanish,sv|Švédština',
  'Default' => 'en',
  'Required' => true,
);

$additionaldomainfields['.events'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.exchange'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.expert'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.exposed'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.express'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fail'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.family'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.farm'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fi'][] = array (
  'Name' => 'identity',
  'DisplayName' => 'ID number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.fi'][] = array (
  'Name' => 'type',
  'DisplayName' => 'Type',
  'Type' => 'dropdown',
  'Options' => ',0|Private person,1|Company,2|Corporation,3|Institution,4|Political party,5|Township,6|Government,7|Public community',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.finance'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.financial'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fish'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fitness'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.flights'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.florist'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.football'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.forsale'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.foundation'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fr'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.fr'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fun'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fund'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.furniture'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.futbol'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.fyi'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gallery'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.games'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ge'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.moscow'][] = array (
  'Name' => 'contact_type',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',I|Individual,O|Legal person',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.moscow'][] = array (
  'Name' => 'date_of_birth',
  'DisplayName' => 'Fyzická osoba - datum narození',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.moscow'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Fyzická osoba - název a číslo identifikačního dokumentu',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.moscow'][] = array (
  'Name' => 'tin',
  'DisplayName' => 'Právnická osoba - ID plátce DPH',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--80adxhks'][] = array (
  'Name' => 'contact_type',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',I|Individual,O|Legal person',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--80adxhks'][] = array (
  'Name' => 'date_of_birth',
  'DisplayName' => 'Fyzická osoba - datum narození',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--80adxhks'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Fyzická osoba - název a číslo identifikačního dokumentu',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--80adxhks'][] = array (
  'Name' => 'tin',
  'DisplayName' => 'Právnická osoba - ID plátce DPH',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gal'][] = array (
  'Name' => 'X-INTENDED-USE',
  'DisplayName' => 'Planned usage of the domain name.',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.gifts'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gives'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.glass'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.global'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',de|German,da|Danish,pl|Polish,se|Švédština,es|Spanish,ar|Arabic,be|Belarusian,ba|Bosna a Hercegovina,bg|Bulgarian,zh-cn|Čínština (zjednodušená),zh-tw|Čínština (tradiční),hi|Hindi,hu|Hungarian,is|Icelandic,ko|Korean,lv|Lotyština,lt|Lithuanian,mk|Makedonština,me|Černohorština,ru|Russian,sr|Serbian,ua|Ukrainian,fr|French,it|Italian,pt|Portuguese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gmbh'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gold'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.golf'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.graphics'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gratis'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.gripe'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.group'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.guide'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.guru'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hamburg'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.haus'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.healthcare'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => 'cttype',
  'DisplayName' => 'Kategorie domény',
  'Type' => 'dropdown',
  'Options' => ',I|Jednotlivec,O|Organization',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '1_cttype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',I|Individual,O|Legal person',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '2_docTypeI',
  'DisplayName' => 'Typ identifikačního dokumentu',
  'Type' => 'dropdown',
  'Options' => ',HKID|Hong Kong Identity Number,OTHID|Other\'s Country Identity Number,PASSNO|Passport No.,BIRTHCERT|Birth Certificate,OTHIDV|Others Individual Document',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '2_docTypeO',
  'DisplayName' => 'Typ identifikačního dokumentu',
  'Type' => 'dropdown',
  'Options' => ',BR|Business Registration Certificate,CI|Certificate of Incorporation,CRS|Certificate of Registration of a School,HKSARG|Hong Kong Special Administrative Region Government Department,HKORDINANCE|Ordinance of Hong Kong,OTHORG|Others Organization Document',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '3_otherDoc',
  'DisplayName' => 'Název jiného dokumentu',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '4_docNum',
  'DisplayName' => 'Číslo dokumentu',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '5_docOriginCC',
  'DisplayName' => 'Dokument vydán ve státu',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '6_industryType',
  'DisplayName' => 'Typ průmyslu',
  'Type' => 'dropdown',
  'Options' => ',0|None,010100|Plastics; Petro-Chemicals; Chemicals - Plastics & Plastic Products,010200|Plastics; Petro-Chemicals; Chemicals - Rubber & Rubber Products,010300|Plastics; Petro-Chemicals; Chemicals - Fibre Materials & Products,010400|Plastics; Petro-Chemicals; Chemicals - Petroleum; Coal & Other Fuels,010500|Plastics; Petro-Chemicals; Chemicals - Chemicals & Chemical Products,020100|Metals; Machinery; Equipment - Metal Materials & Treatment,020200|Metals; Machinery; Equipment - Metal Products,020300|Metals; Machinery; Equipment - Industrial Machinery & Supplies,020400|Metals; Machinery; Equipment - Precision & Optical Equipment,020500|Metals; Machinery; Equipment - Moulds & Dies,030100|Printing; Paper; Publishing - Printing; Photocopying; Publishing,030200|Printing; Paper; Publishing - Paper; Paper Products,040100|Construction; Decoration; Environmental Engineering - Construction Contractors,040200|Construction; Decoration; Environmental Engineering - Construction Materials,040300|Construction; Decoration; Environmental Engineering - Decoration Materials,040400|Construction; Decoration; Environmental Engineering - Construction; Safety Equipment & Supplies,040500|Construction; Decoration; Environmental Engineering - Decoration; Locksmiths; Plumbing & Electrical Works,040600|Construction; Decoration; Environmental Engineering - Fire Protection Equipment & Services,040700|Construction; Decoration; Environmental Engineering - Environmental Engineering; Waste Reduction,050100|Textiles; Clothing & Accessories - Textiles; Fabric,050200|Textiles; Clothing & Accessories - Clothing,050300|Textiles; Clothing & Accessories - Uniforms; Special Clothing,050400|Textiles; Clothing & Accessories - Clothing Manufacturing Accessories,050500|Textiles; Clothing & Accessories - Clothing Processing & Equipment,050600|Textiles; Clothing & Accessories - Fur; Leather & Leather Goods,050700|Textiles; Clothing & Accessories - Handbags; Footwear; Optical Goods; Personal Accessories,060100|Electronics; Electrical Appliances - Electronic Equipment & Supplies,060200|Electronics; Electrical Appliances - Electronic Parts & Components,060300|Electronics; Electrical Appliances - Electrical Appliances; Audio-Visual Equipment,070100|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Kitchenware; Tableware,070200|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Bedding,070300|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Bathroom; Cleaning Accessories,070400|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Household Goods,070500|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Wooden; Bamboo & Rattan Goods,070600|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Home Furnishings; Arts & Crafts,070700|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Watches; Clocks,070800|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Jewellery Accessories,070900|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Toys; Games; Gifts,080100|Business & Professional Services; Finance - Accounting; Legal Services,080200|Business & Professional Services; Finance - Advertising; Promotion Services,080300|Business & Professional Services; Finance - Consultancy Services,080400|Business & Professional Services; Finance - Translation; Design Services,080500|Business & Professional Services; Finance - Cleaning; Pest Control Services,080600|Business & Professional Services; Finance - Security Services,080700|Business & Professional Services; Finance - Trading; Business Services,080800|Business & Professional Services; Finance - Employment Services,080900|Business & Professional Services; Finance - Banking; Finance; Investment,081000|Business & Professional Services; Finance - Insurance,081100|Business & Professional Services; Finance - Property; Real Estate,090100|Transportation; Logistics - Land Transport; Motorcars,090200|Transportation; Logistics - Sea Transport; Boats,090300|Transportation; Logistics - Air Transport,090400|Transportation; Logistics - Moving; Warehousing; Courier & Logistics Services,090500|Transportation; Logistics - Freight Forwarding,100100|Office Equipment; Furniture; Stationery; Information Technology - Office; Commercial Equipment & Supplies,100200|Office Equipment; Furniture; Stationery; Information Technology - Office & Home Furniture,100300|Office Equipment; Furniture; Stationery; Information Technology - Stationery & Educational Supplies,100400|Office Equipment; Furniture; Stationery; Information Technology - Telecommunication Equipment & Services,100500|Office Equipment; Furniture; Stationery; Information Technology - Computers; Information Technology,110100|Food; Flowers; Fishing & Agriculture - Food Products & Supplies,110200|Food; Flowers; Fishing & Agriculture - Beverages; Tobacco,110300|Food; Flowers; Fishing & Agriculture - Restaurant Equipment & Supplies,110400|Food; Flowers; Fishing & Agriculture - Flowers; Artificial Flowers; Plants,110500|Food; Flowers; Fishing & Agriculture - Fishing,110600|Food; Flowers; Fishing & Agriculture - Agriculture,120100|Medical Services; Beauty; Social Services - Medicine & Herbal Products,120200|Medical Services; Beauty; Social Services - Medical & Therapeutic Services,120300|Medical Services; Beauty; Social Services - Medical Equipment & Supplies,120400|Medical Services; Beauty; Social Services - Beauty; Health,120500|Medical Services; Beauty; Social Services - Personal Services,120600|Medical Services; Beauty; Social Services - Organizations; Associations,120700|Medical Services; Beauty; Social Services - Information; Media,120800|Medical Services; Beauty; Social Services - Public Utilities,120900|Medical Services; Beauty; Social Services - Religion; Astrology; Funeral Services,130100|Culture; Education - Music; Arts,130200|Culture; Education - Learning Instruction & Training,130300|Culture; Education - Elementary Education,130400|Culture; Education - Tertiary Education; Other Education Services,130500|Culture; Education - Sporting Goods,130600|Culture; Education - Sporting; Recreational Facilities & Venues,130700|Culture; Education - Hobbies; Recreational Activities,130800|Culture; Education - Pets; Pets Services & Supplies,140101|Dining; Entertainment; Shopping; Travel - Restaurant Guide - Chinese,140102|Dining; Entertainment; Shopping; Travel - Restaurant Guide - Asian,140103|Dining; Entertainment; Shopping; Travel - Restaurant Guide - Western,140200|Dining; Entertainment; Shopping; Travel - Catering Services; Eateries,140300|Dining; Entertainment; Shopping; Travel - Entertainment Venues,140400|Dining; Entertainment; Shopping; Travel - Entertainment Production & Services,140500|Dining; Entertainment; Shopping; Travel - Entertainment Equipment & Facilities,140600|Dining; Entertainment; Shopping; Travel - Shopping Venues,140700|Dining; Entertainment; Shopping; Travel - Travel; Hotels & Accommodation',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hk'][] = array (
  'Name' => '7_under18',
  'DisplayName' => 'Majitel mladší osmnácti let',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => 'cttype',
  'DisplayName' => 'Kategorie domény',
  'Type' => 'dropdown',
  'Options' => ',I|Jednotlivec,O|Organization',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '1_cttype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',I|Individual,O|Legal person',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '2_docTypeI',
  'DisplayName' => 'Typ identifikačního dokumentu',
  'Type' => 'dropdown',
  'Options' => ',HKID|Hong Kong Identity Number,OTHID|Other\'s Country Identity Number,PASSNO|Passport No.,BIRTHCERT|Birth Certificate,OTHIDV|Others Individual Document',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '2_docTypeO',
  'DisplayName' => 'Typ identifikačního dokumentu',
  'Type' => 'dropdown',
  'Options' => ',BR|Business Registration Certificate,CI|Certificate of Incorporation,CRS|Certificate of Registration of a School,HKSARG|Hong Kong Special Administrative Region Government Department,HKORDINANCE|Ordinance of Hong Kong,OTHORG|Others Organization Document',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '3_otherDoc',
  'DisplayName' => 'Název jiného dokumentu',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '4_docNum',
  'DisplayName' => 'Číslo dokumentu',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '5_docOriginCC',
  'DisplayName' => 'Dokument vydán ve státu',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '6_industryType',
  'DisplayName' => 'Typ průmyslu',
  'Type' => 'dropdown',
  'Options' => ',0|None,010100|Plastics; Petro-Chemicals; Chemicals - Plastics & Plastic Products,010200|Plastics; Petro-Chemicals; Chemicals - Rubber & Rubber Products,010300|Plastics; Petro-Chemicals; Chemicals - Fibre Materials & Products,010400|Plastics; Petro-Chemicals; Chemicals - Petroleum; Coal & Other Fuels,010500|Plastics; Petro-Chemicals; Chemicals - Chemicals & Chemical Products,020100|Metals; Machinery; Equipment - Metal Materials & Treatment,020200|Metals; Machinery; Equipment - Metal Products,020300|Metals; Machinery; Equipment - Industrial Machinery & Supplies,020400|Metals; Machinery; Equipment - Precision & Optical Equipment,020500|Metals; Machinery; Equipment - Moulds & Dies,030100|Printing; Paper; Publishing - Printing; Photocopying; Publishing,030200|Printing; Paper; Publishing - Paper; Paper Products,040100|Construction; Decoration; Environmental Engineering - Construction Contractors,040200|Construction; Decoration; Environmental Engineering - Construction Materials,040300|Construction; Decoration; Environmental Engineering - Decoration Materials,040400|Construction; Decoration; Environmental Engineering - Construction; Safety Equipment & Supplies,040500|Construction; Decoration; Environmental Engineering - Decoration; Locksmiths; Plumbing & Electrical Works,040600|Construction; Decoration; Environmental Engineering - Fire Protection Equipment & Services,040700|Construction; Decoration; Environmental Engineering - Environmental Engineering; Waste Reduction,050100|Textiles; Clothing & Accessories - Textiles; Fabric,050200|Textiles; Clothing & Accessories - Clothing,050300|Textiles; Clothing & Accessories - Uniforms; Special Clothing,050400|Textiles; Clothing & Accessories - Clothing Manufacturing Accessories,050500|Textiles; Clothing & Accessories - Clothing Processing & Equipment,050600|Textiles; Clothing & Accessories - Fur; Leather & Leather Goods,050700|Textiles; Clothing & Accessories - Handbags; Footwear; Optical Goods; Personal Accessories,060100|Electronics; Electrical Appliances - Electronic Equipment & Supplies,060200|Electronics; Electrical Appliances - Electronic Parts & Components,060300|Electronics; Electrical Appliances - Electrical Appliances; Audio-Visual Equipment,070100|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Kitchenware; Tableware,070200|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Bedding,070300|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Bathroom; Cleaning Accessories,070400|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Household Goods,070500|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Wooden; Bamboo & Rattan Goods,070600|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Home Furnishings; Arts & Crafts,070700|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Watches; Clocks,070800|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Jewellery Accessories,070900|Houseware; Watches; Clocks; Jewellery; Toys; Gifts - Toys; Games; Gifts,080100|Business & Professional Services; Finance - Accounting; Legal Services,080200|Business & Professional Services; Finance - Advertising; Promotion Services,080300|Business & Professional Services; Finance - Consultancy Services,080400|Business & Professional Services; Finance - Translation; Design Services,080500|Business & Professional Services; Finance - Cleaning; Pest Control Services,080600|Business & Professional Services; Finance - Security Services,080700|Business & Professional Services; Finance - Trading; Business Services,080800|Business & Professional Services; Finance - Employment Services,080900|Business & Professional Services; Finance - Banking; Finance; Investment,081000|Business & Professional Services; Finance - Insurance,081100|Business & Professional Services; Finance - Property; Real Estate,090100|Transportation; Logistics - Land Transport; Motorcars,090200|Transportation; Logistics - Sea Transport; Boats,090300|Transportation; Logistics - Air Transport,090400|Transportation; Logistics - Moving; Warehousing; Courier & Logistics Services,090500|Transportation; Logistics - Freight Forwarding,100100|Office Equipment; Furniture; Stationery; Information Technology - Office; Commercial Equipment & Supplies,100200|Office Equipment; Furniture; Stationery; Information Technology - Office & Home Furniture,100300|Office Equipment; Furniture; Stationery; Information Technology - Stationery & Educational Supplies,100400|Office Equipment; Furniture; Stationery; Information Technology - Telecommunication Equipment & Services,100500|Office Equipment; Furniture; Stationery; Information Technology - Computers; Information Technology,110100|Food; Flowers; Fishing & Agriculture - Food Products & Supplies,110200|Food; Flowers; Fishing & Agriculture - Beverages; Tobacco,110300|Food; Flowers; Fishing & Agriculture - Restaurant Equipment & Supplies,110400|Food; Flowers; Fishing & Agriculture - Flowers; Artificial Flowers; Plants,110500|Food; Flowers; Fishing & Agriculture - Fishing,110600|Food; Flowers; Fishing & Agriculture - Agriculture,120100|Medical Services; Beauty; Social Services - Medicine & Herbal Products,120200|Medical Services; Beauty; Social Services - Medical & Therapeutic Services,120300|Medical Services; Beauty; Social Services - Medical Equipment & Supplies,120400|Medical Services; Beauty; Social Services - Beauty; Health,120500|Medical Services; Beauty; Social Services - Personal Services,120600|Medical Services; Beauty; Social Services - Organizations; Associations,120700|Medical Services; Beauty; Social Services - Information; Media,120800|Medical Services; Beauty; Social Services - Public Utilities,120900|Medical Services; Beauty; Social Services - Religion; Astrology; Funeral Services,130100|Culture; Education - Music; Arts,130200|Culture; Education - Learning Instruction & Training,130300|Culture; Education - Elementary Education,130400|Culture; Education - Tertiary Education; Other Education Services,130500|Culture; Education - Sporting Goods,130600|Culture; Education - Sporting; Recreational Facilities & Venues,130700|Culture; Education - Hobbies; Recreational Activities,130800|Culture; Education - Pets; Pets Services & Supplies,140101|Dining; Entertainment; Shopping; Travel - Restaurant Guide - Chinese,140102|Dining; Entertainment; Shopping; Travel - Restaurant Guide - Asian,140103|Dining; Entertainment; Shopping; Travel - Restaurant Guide - Western,140200|Dining; Entertainment; Shopping; Travel - Catering Services; Eateries,140300|Dining; Entertainment; Shopping; Travel - Entertainment Venues,140400|Dining; Entertainment; Shopping; Travel - Entertainment Production & Services,140500|Dining; Entertainment; Shopping; Travel - Entertainment Equipment & Facilities,140600|Dining; Entertainment; Shopping; Travel - Shopping Venues,140700|Dining; Entertainment; Shopping; Travel - Travel; Hotels & Accommodation',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => '7_under18',
  'DisplayName' => 'Majitel mladší osmnácti let',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.hk'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hockey'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.holdings'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.holiday'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hospital'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.host'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.house'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hr'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.hr'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hu'][] = array (
  'Name' => 'dnssec',
  'DisplayName' => 'DNSSEC',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hu'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/ID/Passport',
  'Type' => 'text',
  'Size' => 10,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.hu'][] = array (
  'Name' => 'statehu',
  'DisplayName' => 'Allowed states',
  'Type' => 'dropdown',
  'Options' => ',EU|European Union / Európai Unió,AD|Andorra / Andorra,AL|Albania / Albánia,AM|Armenia / Örményország,AZ|Azerbaijan / Azerbajdzsán,BA|Bosnia and Herzegovina / Bosznia-Hercegovina,CH|Switzerland / Svájc,GE|Georgia / Grúzia,HR|Croatia / Horvátország,IS|Iceland / Izland,LI|Liechtenstein / Liechtenstein,MC|Monaco / Monaco,MD|Republic of Moldova / Moldova,ME|Montenegro / Montenegró,MK|The former Yugoslav Republic of Macedonia / Macedónia,NO|Norway / Norvégia,RO|Romania / Románia,RS|Serbia / Szerbia,RU|Russian Federation / Oroszország,SM|San Marino / San Marino,TR|Turkey / Törökország,UA|Ukraine / Ukrajna',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.hu'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bolt.hu'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 10,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.bolt.hu'][] = array (
  'Name' => 'trustee_owner',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'radio',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.bolt.hu'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.hu'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.hu'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.chat'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latn|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.cheap'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.church'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.id'][] = array (
  'Name' => 'pouzitiDomeny',
  'DisplayName' => 'Domain use',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.id'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.id'][] = array (
  'Name' => 'pouzitiDomeny',
  'DisplayName' => 'Domain use',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.id'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ie'][] = array (
  'Name' => 'holder_class',
  'DisplayName' => 'Typ držitele domény',
  'Type' => 'dropdown',
  'Options' => ',1|Body Corporate (Ltd;PLC;Company),2|Constitutional Body,3|Discretionary Applicant,4|Natural Person,5|School/Educational Institution,6|Sole Trader,7|Statutory Body,8|Unincorporated Association',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ie'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ie'][] = array (
  'Name' => 'remarks',
  'DisplayName' => 'Užití domény',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.immo'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.immobilien'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.industries'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.info'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',da|Danish,de|German,hu|Hungarian,is|Icelandic,lv|Latvian,lt|Lithuanian,ko|Korean,es|Spanish,sv|Swedish,pl|Polish,zh|Chinese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.institute'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.insure'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.international'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.investments'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.iq'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.iq'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'authority',
  'DisplayName' => 'Company registration center',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'IDCARD-OR-PASSPORT-ISSUER',
  'DisplayName' => 'ID card issuer',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'IDCARD-OR-PASSPORT-NUMBER',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT / ID number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'IR-COMPANY-REGISTRATION-CC',
  'DisplayName' => 'State or province of the Company registration center',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'IR-COMPANY-REGISTRATION-SP',
  'DisplayName' => 'Country of Company registration center',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ir'][] = array (
  'Name' => 'legaltype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',PublicCompany|Public Company,PrivateCompany|Private Company,LimitedCompany|Limited Company,CooperativeCompany|Cooperative Company,Organization|Organization,PressAndPublication|Press and Publication',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'authority',
  'DisplayName' => 'Company registration center',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'IDCARD-OR-PASSPORT-ISSUER',
  'DisplayName' => 'ID card issuer',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'IDCARD-OR-PASSPORT-NUMBER',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT / ID number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'IR-COMPANY-REGISTRATION-CC',
  'DisplayName' => 'State or province of the Company registration center',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'IR-COMPANY-REGISTRATION-SP',
  'DisplayName' => 'Country of Company registration center',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.co.ir'][] = array (
  'Name' => 'legaltype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',PublicCompany|Public Company,PrivateCompany|Private Company,LimitedCompany|Limited Company,CooperativeCompany|Cooperative Company,Organization|Organization,PressAndPublication|Press and Publication',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.it'][] = array (
  'Name' => 'entityType',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',1|Individuals,2|Entrepreneurs and companies from Italy,3|Freelance and professionals from Italy,4|Italian non-profit organizations,5|Italian public organizations,6|Other Italian organizations,7|Subjects 2-6 are not from Italy',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.it'][] = array (
  'Name' => 'nationalityCode',
  'DisplayName' => 'Country',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.it'][] = array (
  'Name' => 'regCode',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.jetzt'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.jewelry'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.jo'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.jobs'][] = array (
  'Name' => 'X-JOBS-COMPANYURL',
  'DisplayName' => 'Web',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.jo'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.kaufen'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.kitchen'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.kr'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.kr'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.krd'][] = array (
  'Name' => 'law-accreditation-b',
  'DisplayName' => 'Accreditation Body',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.krd'][] = array (
  'Name' => 'law-accreditation-id',
  'DisplayName' => 'Accreditation ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.krd'][] = array (
  'Name' => 'law-accredita-year',
  'DisplayName' => 'Accreditation Year',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.krd'][] = array (
  'Name' => 'law-jurisdiction-country',
  'DisplayName' => 'Země Jurisdikce',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.kw'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.land'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.law'][] = array (
  'Name' => 'law-accreditation-b',
  'DisplayName' => 'Accreditation Body',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.law'][] = array (
  'Name' => 'law-accreditation-id',
  'DisplayName' => 'Accreditation ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.law'][] = array (
  'Name' => 'law-accredita-year',
  'DisplayName' => 'Accreditation Year',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.law'][] = array (
  'Name' => 'law-jurisdiction-country',
  'DisplayName' => 'Country Jurisdiction',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.lawyer'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.lb'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.net.lb'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org.lb'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.lease'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.legal'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.life'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.lighting'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.limited'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.limo'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.link'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',Cyrl|Cyrillic,de|German,es|Spanish,fr|French,it|Italian,pt|Portuguese',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.live'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.loans'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.lotto'][] = array (
  'Name' => 'license_number',
  'DisplayName' => 'Number of license',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ltd'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ma'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.ma'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.maison'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.management'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.market'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.marketing'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.mba'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.media'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.memorial'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.mk'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Vyplňte něco z těchto hodnot: IČO, DIČ,  Číslo OP, Číslo pasu.',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.mk'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.moda'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.money'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.mortgage'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.movie'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.my'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.my'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.navy'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.net'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',CZE|Czech,SLO|Slovak,AFR|Afrikaans,ALB|Albanian,ARA|Arabic,ARG|Aragonese,ARM|Armenian,ASM|Assamese,AST|Asturian,AVE|Avestan,AWA|Awadhi,AZE|Azerbaijani,BAN|Balinese,BAL|Baluchi,BAS|Basa,BAK|Bashkir,BAQ|Basque,BEL|Belarusian,BEN|Bengali,BHO|Bhojpuri,BOS|Bosnian,BUL|Bulgarian,BUR|Burmese,CAR|Carib,CAT|Catalan,CHE|Chechen,CHI|Chinese,CHV|Chuvash,COP|Coptic,COS|Corsican,SCR|Croatian,DAN|Danish,DIV|Divehi,DOI|Dogri,DUT|Dutch,ENG|English,EST|Estonian,FAO|Faroese,FIJ|Fijian,FIN|Finnish,FRE|French,FRY|Frisian,GLA|Gaelic,GEO|Georgian,GER|German,GON|Gondi,GUJ|Gujarati,HEB|Hebrew,HIN|Hindi,HUN|Hungarian,ICE|Icelandic,INC|Indic,IND|Indonesian,INH|Ingush,GLE|Irish,ITA|Italian,JPN|Japanese,JAV|Javanese,KAS|Kashmiri,KAZ|Kazakh,KHM|Khmer,KIR|Kirghiz,KOR|Korean,KUR|Kurdish,LAO|Lao,LAV|Latvian,LIT|Lithuanian,LTZ|Luxembourgish,MAC|Macedonian-MAC,MAL|Malayalam,MAY|Malay,MLT|Maltese,MAO|Maori,MOL|Moldavian,MON|Mongolian,NEP|Nepali,NOR|Norwegian,ORI|Oriya,OSS|Ossetian,PAN|Panjabi,PER|Persian,POL|Polish,POR|Portuguese,PUS|Pushto,RAJ|Rajasthani,RUM|Romanian,RUS|Russian,SMO|Samoan,SAN|Sanskrit,SRD|Sardinian,SCC|Serbian,SND|Sindhi,SIN|Sinhalese,SLV|Slovenian,SOM|Somali,SPA|Spanish,SWA|Swahili,SWE|Swedish,SYR|Syriac,TGK|Tajik,TAM|Tamil,TEL|Telugu,THA|Thai,TIB|Tibetan,TUR|Turkish,UKR|Ukrainian,URD|Urdu,UZB|Uzbek,VIE|Vietnamese language,WEL|Welsh,YID|Yiddish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.network'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.news'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ninja'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.no'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.nu'][] = array (
  'Name' => 'admin_idnum',
  'DisplayName' => 'Company ID / Number of ID card / Passport ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.nu'][] = array (
  'Name' => 'admin_vat',
  'DisplayName' => 'VAT',
  'Type' => 'text',
  'Size' => 10,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.nu'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Company ID / Number of ID card / Passport ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.nu'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.observer'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.om'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.om'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.online'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',da|Danish,de|German,hu|Hungarian,is|Icelandic,lv|Latvian,lt|Lithuanian,ko|Korean,es|Spanish,sv|Swedish,pl|Polish,ru|Russian,zh|Čínština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.partners'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.parts'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ph'][] = array (
  'Name' => 'Registrant',
  'DisplayName' => 'Owner',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.photography'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.photos'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.pictures'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.pizza'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.place'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.plumbing'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.plus'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.pm'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.pm'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.porn'][] = array (
  'Name' => 'xxx-defense',
  'DisplayName' => 'Domain blocking',
  'Type' => 'radio',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.porn'][] = array (
  'Name' => 'xxx-memberid',
  'DisplayName' => 'ID člena komunity PORN',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.porn'][] = array (
  'Name' => 'xxx-password',
  'DisplayName' => 'Heslo člena komunity PORN',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.press'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.productions'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.properties'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.pt'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.com.pt'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.pub'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.re'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.re'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.realty'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.recipes'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.rehab'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.reisen'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.rentals'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.repair'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.report'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.republican'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.restaurant'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.reviews'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.rio'][] = array (
  'Name' => 'X-BR-REGISTER-NUMBER',
  'DisplayName' => 'CNPJ/CPF',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.rip'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ro'][] = array (
  'Name' => 'idnumber',
  'DisplayName' => 'VAT / ID number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.rocks'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.ro'][] = array (
  'Name' => 'idnumber',
  'DisplayName' => 'VAT / ID number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.rs'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.rs'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.rs'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.rs'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.edu.rs'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.edu.rs'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.in.rs'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.in.rs'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org.rs'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org.rs'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ru'][] = array (
  'Name' => 'birthdate',
  'DisplayName' => 'Date of birth',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ru'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ruhr'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.run'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.spb.ru'][] = array (
  'Name' => 'birthdate',
  'DisplayName' => 'Date of birth',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.spb.ru'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.sale'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.salon'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.sarl'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.scot'][] = array (
  'Name' => 'x-intended-use',
  'DisplayName' => 'Intended use',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.se'][] = array (
  'Name' => 'admin_idnum',
  'DisplayName' => 'Company ID / Number of ID card / Passport ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.se'][] = array (
  'Name' => 'admin_vat',
  'DisplayName' => 'VAT',
  'Type' => 'text',
  'Size' => 10,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.se'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Company ID / Number of ID card / Passport ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.se'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT',
  'Type' => 'text',
  'Size' => 10,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.services'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.sg'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.com.sg'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.shoes'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.shop'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',ar|Arabština / Arabic,zh|Čínsština / Chinese,nl|Holandština / Dutch,fr|Francouzština / French,de|Němčina / German,ja|Japonština / Japanese,ko|Korejština / Korean,pl|Polština / Polish,po|Portugalština / Portugese,es|Španělština / Spanish,ru|Ruština / Russian,tr|Turečtina / Turkish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.shopping'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.show'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.school'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.schule'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.singles'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.site'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.sk'][] = array (
  'Name' => 'sknicid',
  'DisplayName' => 'SK-NIC ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.sk'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.soccer'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.social'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.software'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.solar'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.solutions'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.space'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.store'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.studio'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.style'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.su'][] = array (
  'Name' => 'birthdate',
  'DisplayName' => 'Date of birth',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.su'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.supplies'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.supply'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.support'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.surgery'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.systems'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tax'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.taxi'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.team'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tech'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.technology'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tennis'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tf'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.tf'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.theater'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.in.th'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tienda'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tips'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tires'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.today'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tools'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.top'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',ARA|Arabic,CHI|Chinese,FRE|French,GER|German,JPN|Japanese,RUS|Russian,SPA|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.tours'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.town'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.toys'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.training'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.travel'][] = array (
  'Name' => 'uin',
  'DisplayName' => 'Unique Identification Number (UIN)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.ua'][] = array (
  'Name' => 'trademark',
  'DisplayName' => 'Trademark',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.uk'][] = array (
  'Name' => 'co-no',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.uk'][] = array (
  'Name' => 'uktype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',LTD|UK Limited Company,PLC|UK Public Limited Company,PTNR|UK Partnership,STRA|UK Sole Trader,LLP|UK Limited Liability Partnership,IP|UK Industrial/Provident Registered Company,IND|UK Individual (representing self),SCH|UK School,RCHAR|UK Registered Charity,GOV|UK Government Body,CRC|UK Corporation by Royal Charter,STAT|UK Statutory Body,OTHER|UK Entity that does not fit into any of the above (e.g. clubs; associations; many universities),FIND|Non-UK Individual (representing self),FCORP|Non-UK Corporation,FOTHER|Non-UK Entity that does not fit into any of the above (e.g. charities; schools; clubs; associations)',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.uk'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.uk'][] = array (
  'Name' => 'co-no',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.co.uk'][] = array (
  'Name' => 'uktype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',LTD|UK Limited Company,PLC|UK Public Limited Company,PTNR|UK Partnership,STRA|UK Sole Trader,LLP|UK Limited Liability Partnership,IP|UK Industrial/Provident Registered Company,IND|UK Individual (representing self),SCH|UK School,RCHAR|UK Registered Charity,GOV|UK Government Body,CRC|UK Corporation by Royal Charter,STAT|UK Statutory Body,OTHER|UK Entity that does not fit into any of the above (e.g. clubs; associations; many universities),FIND|Non-UK Individual (representing self),FCORP|Non-UK Corporation,FOTHER|Non-UK Entity that does not fit into any of the above (e.g. charities; schools; clubs; associations)',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.me.uk'][] = array (
  'Name' => 'co-no',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.me.uk'][] = array (
  'Name' => 'uktype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',LTD|UK Limited Company,PLC|UK Public Limited Company,PTNR|UK Partnership,STRA|UK Sole Trader,LLP|UK Limited Liability Partnership,IP|UK Industrial/Provident Registered Company,IND|UK Individual (representing self),SCH|UK School,RCHAR|UK Registered Charity,GOV|UK Government Body,CRC|UK Corporation by Royal Charter,STAT|UK Statutory Body,OTHER|UK Entity that does not fit into any of the above (e.g. clubs; associations; many universities),FIND|Non-UK Individual (representing self),FCORP|Non-UK Corporation,FOTHER|Non-UK Entity that does not fit into any of the above (e.g. charities; schools; clubs; associations)',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.org.uk'][] = array (
  'Name' => 'co-no',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.org.uk'][] = array (
  'Name' => 'uktype',
  'DisplayName' => 'Owner type',
  'Type' => 'dropdown',
  'Options' => ',LTD|UK Limited Company,PLC|UK Public Limited Company,PTNR|UK Partnership,STRA|UK Sole Trader,LLP|UK Limited Liability Partnership,IP|UK Industrial/Provident Registered Company,IND|UK Individual (representing self),SCH|UK School,RCHAR|UK Registered Charity,GOV|UK Government Body,CRC|UK Corporation by Royal Charter,STAT|UK Statutory Body,OTHER|UK Entity that does not fit into any of the above (e.g. clubs; associations; many universities),FIND|Non-UK Individual (representing self),FCORP|Non-UK Corporation,FOTHER|Non-UK Entity that does not fit into any of the above (e.g. charities; schools; clubs; associations)',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.university'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.us'][] = array (
  'Name' => 'countrycode',
  'DisplayName' => 'Country',
  'Type' => 'dropdown',
  'Options' => 'AF|Afghanistan,AL|Albania,DZ|Algeria,AS|American Samoa,AD|Andorra,AO|Angola,AI|Anguilla,AQ|Antarctica,AG|Antigua and Barbuda,AR|Argentina,AM|Armenia,AW|Aruba,AU|Australia,AT|Austria,AZ|Azerbaijan,BS|Bahamas,BH|Bahrain,BD|Bangladesh,BB|Barbados,BY|Belarus,BE|Belgium,BZ|Belize,BJ|Benin,BM|Bermuda,BT|Bhutan,BO|Bolivia,BA|Bosnia and Herzegovina,BW|Botswana,BV|Bouvet Island,BR|Brazil,IO|British Indian Ocean,BN|Brunei Darussalam,BG|Bulgaria,BF|Burkina Faso,BI|Burundi,KH|Cambodia,CM|Cameroon,CA|Canada,CV|Cape Verde,KY|Cayman Islands,CF|Central African Republic,TD|Chad,CL|Chile,CN|China,CX|Christmas Island,CC|Cocos (Keeling) Islands,CO|Colombia,KM|Comoros,CG|Congo,CD|Congo, Democratic Republic of the,CK|Cook Islands,CR|Costa Rica,HR|Croatia,CU|Cuba,CY|Cyprus,CZ|Czech Republic,DK|Denmark,DJ|Djibouti,DM|Dominica,DO|Dominican Republic,TL|East Timor (provisional),EC|Ecuador,EG|Egypt,SV|El Salvador,GQ|Equatorial Guinea,ER|Eritrea,EE|Estonia,ET|Ethiopia,FK|Falkland Islands (Malvinas),FO|Faroe Islands,FJ|Fiji,FI|Finland,FR|France,FR|France, Metropolitan,GF|French Guiana,PF|French Polynesia,TF|French Southern Territories,GA|Gabon,GM|Gambia,GE|Georgia,DE|Germany,GH|Ghana,GI|Gibraltar,GR|Greece,GL|Greenland,GD|Grenada,GP|Guadeloupe,GU|Guam,GT|Guatemala,GN|Guinea,GW|Guinea-Bissau,GY|Guyana,HT|Haiti,HM|Heard Island and McDonald Islands,HN|Honduras,HK|Hong Kong,HU|Hungary,IS|Iceland,IN|India,ID|Indonesia,IR|Iran,IQ|Iraq,IE|Ireland,IL|Israel,IT|Italy,JM|Jamaica,JP|Japan,JO|Jordan,KZ|Kazakhstan,KE|Kenya,KI|Kiribati,KP|Korea, North,KR|Korea, South,KW|Kuwait,KG|Kyrgyzstan,LA|Laos,LV|Latvia,LB|Lebanon,LS|Lesotho,LR|Liberia,LY|Libya,LI|Liechtenstein,LT|Lithuania,LU|Luxembourg,MO|Macau,MK|Macedonia,MG|Madagascar,MW|Malawi,MY|Malaysia,MV|Maldives,ML|Mali,MT|Malta,MH|Marshall Islands,MQ|Martinique,MR|Mauritania,MU|Mauritius,YT|Mayotte,MX|Mexico,FM|Micronesia,MD|Moldova,MC|Monaco,MN|Mongolia,ME|Montenegro,MS|Montserrat,MA|Morocco,MZ|Mozambique,MM|Myanmar,NA|Namibia,NR|Nauru,NP|Nepal,NL|Netherlands,AN|Netherlands Antilles,NC|New Caledonia,NZ|New Zealand,NI|Nicaragua,NE|Niger,NG|Nigeria,NU|Niue,NF|Norfolk Island,MP|Northern Mariana Islands,NO|Norway,OM|Oman,PK|Pakistan,PW|Palau,PA|Panama,PG|Papua New Guinea,PY|Paraguay,PE|Peru,PH|Philippines,PN|Pitcairn,PL|Poland,PT|Portugal,PR|Puerto Rico,QA|Qatar,RE|Reunion,RO|Romania,RU|Russian Federation,RW|Rwanda,SH|Saint Helena,KN|Saint Kitts and Nevis,LC|Saint Lucia,PM|Saint Pierre and Miquelon,VC|Saint Vincent and the Grenadines,WS|Samoa,SM|San Marino,ST|Sao Tome and Principe,SA|Saudi Arabia,SN|Senegal,RS|Serbia,SC|Seychelles,SL|Sierra Leone,SG|Singapore,SK|Slovakia,SI|Slovenia,SB|Solomon Islands,SO|Somalia,ZA|South Africa,GS|South Georgia and Sandwich Isl.,ES|Spain,LK|Sri Lanka,SD|Sudan,SR|Suriname,SJ|Svalbard and Jan Mayen,SZ|Swaziland,SE|Sweden,CH|Switzerland,SY|Syria,TW|Taiwan,TJ|Tajikistan,TZ|Tanzania,TH|Thailand,TG|Togo,TK|Tokelau,TO|Tonga,TT|Trinidad and Tobago,TN|Tunisia,TR|Turkey,TM|Turkmenistan,TC|Turks and Caicos Islands,TV|Tuvalu,UG|Uganda,UA|Ukraine,AE|United Arab Emirates,GB|United Kingdom,US|United States,UM|United States Minor Outlying Islands,UY|Uruguay,UZ|Uzbekistan,VU|Vanuatu,VA|Vatican City State (Holy See),VE|Venezuela,VN|Viet Nam,VG|Virgin Islands, British,VI|Virgin Islands, U.S.,WF|Wallis and Fortuna Islands,WS|Western Sahara (provisional),YE|Yemen,YU|Yugoslavia,ZM|Zambia,ZW|Zimbabwe',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.us'][] = array (
  'Name' => 'us-nexus-apppurpose',
  'DisplayName' => 'Purpose of use',
  'Type' => 'dropdown',
  'Options' => ',P1|Business use for profit,P2|Non-profit business; club; association; religious organization; etc.,P3|For personal use,P4|For education purposes,P5|For government purposes',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.us'][] = array (
  'Name' => 'us-nexus-category',
  'DisplayName' => 'NEXUS Category',
  'Type' => 'dropdown',
  'Options' => ',C11|I am a U.S. citizen.,C12|Who is a permanent resident of the United States of America or any of its posessions or territories.,C21|U.S. Organization incorporated within one of the 50 states or the U.S. Territory,C31|Regularly engages in lawful activities (sales of goods or services or other business; commercial or non-commercial; including not-for-profit relations in the United States).,C32|A subject that has an office or other facility in the USA.',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.vacations'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.ventures'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.vet'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.viajes'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.video'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.villas'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.vin'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.vision'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.voyage'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.watch'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.website'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',latn|latin / latinka,ko|korean / korejština,th|thai / thajština,ja|japan / japonština,cyrl|cyrillic / cyrilika,grek|greek / řečtina,he|hebrew / hebrejština,lo|lao / laoština,ar|arabic / arabština',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.wf'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.wf'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.wine'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.works'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.world'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.wtf'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--e1a4c'][] = array (
  'Name' => 'lang_eu',
  'DisplayName' => 'Language:',
  'Type' => 'dropdown',
  'Options' => 'bg|Bulgarian',
  'Default' => 'en',
  'Required' => true,
);

$additionaldomainfields['.xn--p1ai'][] = array (
  'Name' => 'birthdate',
  'DisplayName' => 'Date of birth',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--p1ai'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--vhquv'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--90a3ac'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--90a3ac'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--c1avg.xn--90a3ac'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--c1avg.xn--90a3ac'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--d1at.xn--90a3ac'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--d1at.xn--90a3ac'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--o1ac.xn--90a3ac'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--o1ac.xn--90a3ac'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xn--90azh.xn--90a3ac'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'Identification number',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xn--90azh.xn--90a3ac'][] = array (
  'Name' => 'vat',
  'DisplayName' => 'VAT (PIB)',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xxx'][] = array (
  'Name' => 'xxx-defense',
  'DisplayName' => 'Domain blocking',
  'Type' => 'radio',
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.xxx'][] = array (
  'Name' => 'xxx-memberid',
  'DisplayName' => 'XXX Member ID',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xxx'][] = array (
  'Name' => 'xxx-password',
  'DisplayName' => 'XXX Member Password',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.xyz'][] = array (
  'Name' => 'lang',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',ja|japan / japonština,he|hebrew / hebrejština,ar|arabic / arabština,ru|russian / ruština,th|thai / thajština,lo|lao / laoština,zh|chinese / čínština,ko|korean / korejština,latn|latin / latinka,grek|greek / řečtina',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.yt'][] = array (
  'Name' => 'idnum',
  'DisplayName' => 'VAT/Passport',
  'Type' => 'text',
  'Size' => 20,
  'Default' => '',
  'Required' => true,
);

$additionaldomainfields['.yt'][] = array (
  'Name' => 'trustee',
  'DisplayName' => 'Local contact (Trustee)',
  'Type' => 'tickbox',
  'Default' => '',
  'Required' => false,
);

$additionaldomainfields['.zone'][] = array (
  'Name' => 'lang_info',
  'DisplayName' => 'IDN Language',
  'Type' => 'dropdown',
  'Options' => ',arab|Arabic,cyrl|Cyrillic,deva|Devanagari,grek|Greek,hebr|Hebrew,ja|Japanese,ko|Korean,latnq|Latin,taml|Tamil,thai|Thai,de|German,zh|Chinese,fr|French,es|Spanish',
  'Default' => '',
  'Required' => false,
);